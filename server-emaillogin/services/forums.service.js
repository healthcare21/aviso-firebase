﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('forums');

var service = {};

service.getAllforums = getAllforums;
service.getById = getById;
service.update = update;
service.create = create;
service.createGroup = createGroup;
service.getGroups = getGroups;
service.updateGroup = updateGroup;
service.createUpload = createUpload;
service.getUploads = getUploads;

module.exports = service;


function create(forumPosts) {
    var deferred = Q.defer();
        // add hashed password to user object
        db.forums.insert(
            forumPosts,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    return deferred.promise;
}

function createUpload(forumPosts) {
    db.bind('uploads');
    var deferred = Q.defer();
        // add hashed password to user object
        db.uploads.insert(
            forumPosts,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    return deferred.promise;
}

function getUploads() {
    var deferred = Q.defer();
    db.bind('uploads');
    db.uploads.find().toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        // return users (without hashed passwords)
        deferred.resolve(events);
    });

    return deferred.promise;
}




function createGroup(groups) {
    var deferred = Q.defer();
    db.bind('groups');
        // add hashed password to user object
        db.groups.insert(
            groups,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    return deferred.promise;
}
function getGroups() {
    var deferred = Q.defer();
    db.bind('groups');
    db.groups.find().toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        // return users (without hashed passwords)
        deferred.resolve(events);
    });

    return deferred.promise;
}


function updateGroup(_id, forumPosts) {
    var deferred = Q.defer();
    db.bind('groups');
    db.groups.findById(_id, function (err, events) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      console.log(events);
      updateGroupI();
    })
    function updateGroupI() {
        // fields to update
        var set = {
            date: forumPosts.date,
            title: forumPosts.title,
        };
        db.groups.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve();
            });
    }
    return deferred.promise;
}

function getAllforums() {
    var deferred = Q.defer();
    db.forums.find().toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        // return users (without hashed passwords)
        deferred.resolve(events);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();
    db.forums.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });
    return deferred.promise;
}


function update(_id, forumPosts) {
    var deferred = Q.defer();
    db.forums.findById(_id, function (err, events) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      updateForum();
    })
    function updateForum() {
        // fields to update
        var set = {
            date: forumPosts.date,
            name: forumPosts.name,
            username: forumPosts.username,
            question: forumPosts.question,
            closeForum: forumPosts.closeForum,
            replies: forumPosts.replies,
            likes: forumPosts.likes,
            groups: forumPosts.groups
        };
        db.forums.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve();
            });
    }
    return deferred.promise;
}
