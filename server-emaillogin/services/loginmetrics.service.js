﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('loginmetrics');

var service = {};

service.createid = createid;
service.getAllforumIDS = getAllforumIDS;
module.exports = service;

function createid(loginmetrics) {
    //console.log(loginmetrics);
    var deferred = Q.defer();
        // add hashed password to user object
        db.loginmetrics.insert(
            loginmetrics,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    return deferred.promise;
}

function getAllforumIDS() {
    var deferred = Q.defer();
    db.loginmetrics.find().toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        // return users (without hashed passwords)
        deferred.resolve(events);
    });
    return deferred.promise;
}
