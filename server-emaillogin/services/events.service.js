﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('events');

var service = {};

service.getAllevents = getAllevents;
service.getById = getById;
service.update = update;
module.exports = service;


function getAllevents() {
    var deferred = Q.defer();
    db.events.find().toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        // return users (without hashed passwords)
        deferred.resolve(events);
    });
    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();
    db.events.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });
    return deferred.promise;
}


function update(_id, userParam) {
    var deferred = Q.defer();
    db.events.findById(_id, function (err, events) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      updateUser();
    })
    function updateUser() {
        // fields to update
        var set = {
            name: userParam.name,
            location: userParam.location,
            webex: userParam.webex,
            eventtext: userParam.eventtext,
            loginText: userParam.loginText,
            usefulLinks: userParam.usefulLinks,
            inmoderation: userParam.inmoderation,
            enddate: userParam.enddate,
            wordlist: userParam.wordlist,
            resources: userParam.resources
            // authority: userParam.authority
        };
        db.events.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve();
            });
    }
    return deferred.promise;
}
