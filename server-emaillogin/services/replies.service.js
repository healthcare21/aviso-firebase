﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('replies');

var service = {};

service.allreplies = getAllreplies;
service.createreply = createreply;
service.getByforumId = getByforumId;
service.updateReply = updateReply;
service.getAllreplies = getAllreplies;
module.exports = service;


function createreply(forumPosts) {
    var deferred = Q.defer();
        // add hashed password to user object
        db.replies.insert(
            forumPosts,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    return deferred.promise;
}


function getAllreplies() {
    var deferred = Q.defer();
    db.replies.find().toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        deferred.resolve(events);
    });

    return deferred.promise;
}

function getByforumId(forumIdvar) {
    var deferred = Q.defer();
      db.replies.find({ forumId : forumIdvar}).toArray(function (err, events) {
        if (err) deferred.reject(err.name + ': ' + err.message);

          deferred.resolve(events);
    });
    return deferred.promise;
}

function updateReply(_id, forumPosts) {
    var deferred = Q.defer();
    db.replies.findById(_id, function (err, events) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      updateForum();
    })
    function updateForum() {
        // fields to update
        var set = {
            date: forumPosts.date,
            name: forumPosts.name,
            username: forumPosts.username,
            question: forumPosts.question,
            reply: forumPosts.reply,
            forumlabel: forumPosts.forumlabel,
            likes: forumPosts.likes
        };
        db.replies.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve();
            });
    }
    return deferred.promise;
}
