﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('users');

var service = {};

service.authenticate = authenticate;
service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.updatePassword = updatePassword;
service.delete = _delete;
service.getByemail =getByemail;

module.exports = service;

function authenticate(username, password) {
    var deferred = Q.defer();
    db.users.findOne({ username: username }, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        if (user && bcrypt.compareSync(password, user.hash)) {
            deferred.resolve({
                _id: user._id,
                title: user.title,
                username: user.username,
                firstName: user.firstName,
                email: user.email,
                lastName: user.lastName,
                authority: user.authority,
                eventid: user.eventid,
                biography: user.biography,
                avatar: user.avatar,
                loggedin: true,
                optOut: user.optOut,
                group: user.group,
                token: jwt.sign({ sub: user._id }, config.secret)
            });
        } else {
            //Check authentication by email
            db.users.findOne({ email: username }, function (err, user) {

                if (err) deferred.reject(err.name + ': ' + err.message);
                if (user && bcrypt.compareSync(password, user.hash)) {
                    // authentication successful

                    deferred.resolve({
                        _id: user._id,
                        username: user.username,
                        firstName: user.firstName,
                        email: user.email,
                        lastName: user.lastName,
                        authority: user.authority,
                        eventid: user.eventid,
                        biography: user.biography,
                        avatar: user.avatar,
                        loggedin: true,
                        optOut: user.optOut,
                        group: user.group,
                        token: jwt.sign({ sub: user._id }, config.secret)
                    });
                  } else {
                    // authentication failed
                    deferred.resolve();
                  }
                });


        }
    });

    return deferred.promise;
}

function getAll() {
    var deferred = Q.defer();
    db.users.find().toArray(function (err, users) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        // return users (without hashed passwords)
        users = _.map(users, function (user) {
            return _.omit(user, 'hash');
        });
        deferred.resolve(users);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user) {
            // return user (without hashed password)
            //deferred.resolve(_.omit(user, 'hash'));
            deferred.resolve(user); // Send hashed password as we need this for admin duties.
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function updatePassword(_id,randomPassword) {
    var deferred = Q.defer();
    db.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        if (user) {
            // return user (without hashed password)
            //Set password to random
            // update password if it was entered
            var set = {};
            set.hash = bcrypt.hashSync(randomPassword, 10);
            db.users.update(
                { _id: mongo.helper.toObjectID(_id) },
                { $set: set },
                function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve();
                });
        } else {
            // user not found
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function getByemail(email) {
    var deferred = Q.defer();
    db.users.findOne({'email' : email}, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });
    return deferred.promise;
}


function create(userParam) {
    var deferred = Q.defer();
    // validation
    db.users.findOne(
        { username: userParam.email },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (user) {
                // username already exists
                deferred.reject('Username "' + userParam.username + '" is already taken');
            } else {
                createUser();
            }
        });

    function createUser() {
        // set user object to userParam without the cleartext password
        var user = _.omit(userParam, 'password');
        str = JSON.stringify(userParam);
        // add hashed password to user object
        user.hash = bcrypt.hashSync(userParam.password, 10);
        db.users.insert(
            user,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(_id, userParam) {
    var deferred = Q.defer();
    // validation
    db.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        if(user){
        if (user.username !== userParam.username) {
            // username has changed so check if the new username is already taken
            db.users.findOne(
                { username: userParam.username },
                function (err, user) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    if (user) {
                        // username already exists
                        deferred.reject('Username "' + req.body.username + '" is already taken')
                    } else {
                        updateUser();
                    }
                });
        } else {
            updateUser();
        }
      }
    });

    function updateUser() {
        var set = {
            title: userParam.title,
            username: userParam.username,
            firstName: userParam.firstName,
            email: userParam.email,
            lastName: userParam.lastName,
            authority: userParam.authority,
            optOut: userParam.optOut,
            biography: userParam.biography,
            eventid: userParam.eventid,
            avatar: userParam.avatar,
            loggedin: userParam.loggedin,
            optOut: userParam.optOut,
            group: userParam.group,
            token: jwt.sign({ sub: userParam._id }, config.secret)
        };

        // update password if it was entered
        if (userParam.password) {
            set.hash = bcrypt.hashSync(userParam.password, 10);
        }

        db.users.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.users.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
