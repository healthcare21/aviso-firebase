﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var loginmetrics = require('services/loginmetrics.service');

// routes


router.post('/create', create);
router.get('/getAllforumIDS', getAllforumIDS);

module.exports = router;

function create(req, res) {
  // console.log(req.params);
    loginmetrics.createid(req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getAllforumIDS(req, res) {
    loginmetrics.getAllforumIDS()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
