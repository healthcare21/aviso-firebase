﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var repliesService = require('services/replies.service');

const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
const TOKEN_PATH = 'token.json';

// routes

router.get('/allreplies', getAllreplies);
router.post('/createreply', createreply);
router.get('/getByforumId/:forumId', getByforumId);
router.post('/updateReply', updateReply );

module.exports = router;

function updateReply(req, res) {
    repliesService.updateReply(req.body._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getAllreplies(req, res) {

    repliesService.getAllreplies()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function createreply(req, res) {
    repliesService.createreply(req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getByforumId(req, res) {

    repliesService.getByforumId(req.params.forumId)
        .then(function (events) {
          //  console.log('getByforumId  '+events);
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
