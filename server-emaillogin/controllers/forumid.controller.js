﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var forumid = require('services/forumid.service');

// routes


router.get('/create/:id', createid);
router.get('/getAllforumIDS', getAllforumIDS);

module.exports = router;

function createid(req, res) {
  // console.log(req.params);
    forumid.createid(req.params)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getAllforumIDS(req, res) {
    forumid.getAllforumIDS()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
