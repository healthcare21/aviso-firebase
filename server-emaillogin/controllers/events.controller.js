﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var eventsServive = require('services/events.service');

const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
const TOKEN_PATH = 'token.json';

// routes

router.get('/allevents', getAllevents);
router.get('/single/:_id', getSingle);
router.put('/update/:_id', updateEvents);

module.exports = router;

function updateEvents(req, res) {
  console.log(req.body);
    eventsServive.update(req.body._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function getAllevents(req, res) {

    eventsServive.getAllevents()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getSingle(req, res) {
  //console.log('getSingle'+req.params._id);
    eventsServive.getById(req.params._id)
        .then(function (event) {
            if (event) {
                res.send(event);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
