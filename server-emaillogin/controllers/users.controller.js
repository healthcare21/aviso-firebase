﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var userService = require('services/user.service');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/:_id', getAll);
router.get('/current', getCurrent);
router.get('/single/:_id', getSingle);
router.get('/byemail/:email', getByemail);
router.put('/updatePassword', updatePassword);
router.put('/:_id', update);
router.delete('/:_id', _delete);

module.exports = router;

function updatePassword(req, res) {
    console.log(req.body.password);
    if(req.body.password != 'random'){
      var random = req.body.password;
    } else {
      var random = randomPassword(6);
    }
    //console.log(random);
    userService.updatePassword(req.body._id, random)
        .then(function () {
            res.json(random);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function authenticate(req, res) {
    userService.authenticate(req.body.username, req.body.password)
        .then(function (user) {
            if (user) {
                // authentication successful
                res.send(user);
            } else {
                // authentication failed
                res.status(400).send('Username or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function register(req, res) {
    userService.create(req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getAll(req, res) {
    userService.getAll()
        .then(function (users) {
            res.send(users);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getCurrent(req, res) {
    userService.getById(req.user.sub)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
function getByemail(req, res) {
    userService.getByemail(req.params.email)
        .then(function (user) {
            if (user) {
                res.send('[{ "notknown": "'+user._id+'" }]');
            } else {
              //  res.sendStatus(404);
                res.send('[{ "notknown": "true" }]');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
            res.send([{ "notknown": "true" }]);
        });
}

function update(req, res) {
    userService.update(req.params._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
function getSingle(req, res) {
    userService.getById(req.params._id)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function _delete(req, res) {
    userService.delete(req.params._id)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
