﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var forumservice = require('services/forums.service');

// routes

router.get('/getAllforums', getAllforumscontroll);
router.post('/create', create);
router.post('/update', updateForum );
router.get('/getSingle/:_id', getSingle);
router.post('/createGroup', createGroup);
router.get('/getGroups', getGroups);
router.get('/getUploads', getUploads);
router.post('/updateGroup', updateGroup );
router.post('/createUpload', createUpload);

module.exports = router;

function create(req, res) {
    forumservice.create(req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function createUpload(req, res) {
    forumservice.createUpload(req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function createGroup(req, res) {
    forumservice.createGroup(req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getGroups(req, res) {
    forumservice.getGroups()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getUploads(req, res) {
    forumservice.getUploads()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function updateGroup(req, res) {
    forumservice.updateGroup(req.body._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function updateForum(req, res) {
    forumservice.update(req.body._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function getAllforumscontroll(req, res) {
    forumservice.getAllforums()
        .then(function (events) {
            res.send(events);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getSingle(req, res) {
    forumservice.getById(req.params._id)
        .then(function (event) {
            if (event) {
                res.send(event);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
