﻿require('rootpath')();
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var config = require('config.json');
var multer = require('multer');
var morgan  = require('morgan');
const path = require('path');


var nodeMailer = require('nodemailer');
var DIR = './uploads/';

//Multer upload file
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + '' + path.extname(file.originalname));
    }
});
let upload = multer({storage: storage});


app.use(cors());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json({limit: '2mb'}));
app.use(express.urlencoded({limit: '2mb'}));
app.use(morgan('dev'));
// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
// app.use(expressJwt({
//     secret: config.secret,
//     getToken: function (req) {
//         if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
//             return req.headers.authorization.split(' ')[1];
//         } else if (req.query && req.query.token) {
//             return req.query.token;
//         }
//         return null;
//     }
// }).unless({ path: ['/users/authenticate', '/users/register', '/send-email', '/users/updatePassword', /^\/users\/byemail\/.*/, , /^\/users\/updatePassword\/.*/ ] }));

// routes
app.use('/users', require('./controllers/users.controller'));
app.use('/events', require('./controllers/events.controller'));
app.use('/forums', require('./controllers/forums.controller'));
app.use('/forumid', require('./controllers/forumid.controller'));
app.use('/loginmetrics', require('./controllers/loginmetrics.controller'));
app.use('/replies', require('./controllers/replies.controller'));
// error handler
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send('Invalid Token');
    } else {
        throw err;
    }
});

app.use(function(req, res, next) {
//set headers to allow cross origin request.
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/api/upload',upload.single('photo'), function (req, res) {
    if (!req.file) {
        console.log("No file received");
        return res.send({
          success: false
        });
    
      } else {
        console.log('file received');
        return res.send({
          filename: req.file.filename
        })
      }
});
app.get ('/api/download/:_id', function (req, res, next) {
    let file = req.params._id;
    var paths = path.resolve(".") + '/uploads/' + file;
    res.download(paths);
  })



app.options('/send-email', function (req, res) {
  res.sendStatus(200);
});

let transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'webape2@gmail.com',
        pass: 'Hellsgrandads99'
    }
});

app.post('/send-email', function (req, res) {
      console.log('sendemail');

  let mailOptions = {
      from: '"Virtual ad board" <matt.burton@healthcare21.co.uk>', // sender address
      to: req.body.to, // list of receivers
      subject: req.body.subject, // Subject line
      text: req.body.body, // plain text body
      html: req.body.body // html text body????
  };

  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
          console.log('Message %s sent: %s', info.messageId, info.response);
          return ("Email sent");
      });
      res.json(req.body);

  });

   


// start server
var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
