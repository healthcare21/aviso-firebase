var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
const ftp = require( 'vinyl-ftp' );
const sftp = require('gulp-sftp');


// task for deploying files on the server
gulp.task('deploy', function() {
    console.log('SFTP');
    const config = require('./sftp-config.json');

    const globs = [
        '../*', '!../gulp','../!gulp','../assets/icheck/*','../assets/js/*','../components/**','../functions/**','../templates/**'
    ];

    if (config.type == 'ftp') {
        //  FTP version
        const conn = ftp.create( {
            host:     config.host,
            user:     config.user,
            password: config.password,
            port:     config.port,
            parallel: 10,
            remotePath: config.remote_path,
            reload:   true,
            // debug:    function(d){console.log(d);}
            
        });
        return gulp.src( globs, { base: '/dist', buffer: false } )
            .pipe( conn.newer( '/aviso.healthcare21.website' ) )
            .pipe( conn.dest( '/aviso.healthcare21.website' ) )
            // .pipe(slack('Deployed Aviso to aviso.healthcare21.website'));
    } else {
        // SFTP version
        const conn = sftp({
                host: config.host,
                user: config.user,
                pass: config.password,
                port: config.port,
                remotePath: config.remote_path,
            });
            // gulp.src('app/**/*').pipe(conn)
        return gulp.src('../404.php' )
            .pipe(conn)
            .pipe(slack('Deployed latest build Gulp Test'));
    }
});
