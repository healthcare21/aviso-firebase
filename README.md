# Virtual ad board
#healthcare21 #work
SSH to digital ocean
ssh root@165.227.236.46

Git repository: [Bitbucket](https://bitbucket.org/healthcare21/virtual-advisory-rebase/src)

Start express server:
`node/server/server.js`

Start Angular (locally)
`npm start`

Build angular
`ng build -prod`

Before build need to change the variable at /app.config.ts:
```
export const appConfig = {
    apiUrl: 'http://localhost:4000'
};
```
To
```
export const appConfig = {
    apiUrl: 'http://165.227.236.46:4000'
};
```

Then ng build -prod

## Dump and Restore database:

**Restore db on remote /ssh server.**
`mongorestore --db mean-angular2-registration-login-example  dump/mean-angular2-registration-login-example/`

**Dump database**
`mongodump  --db mean-angular2-registration-login-example`

```
mongodump  --db mean-angular2-registration-login-example --username webUser --password abc123
```
Dump database with username and password.


Upload this to the remote server and run the restore script.

**Creating default mongo users**

`Use mean-angular2-registration-login-example`
`db.createUser({ user: "webUser", pwd: "abc123", roles: [{ role: "readWrite", db: "mean-angular2-registration-login-example"}] })`

**Changing global variables**

The application runs a global variable script at  /globals.ts this is imported to components as:
`import { Globals } from '../globals';`

You can change:
sitename - eg. Virtual Advisory Board
siteslogan - eg. Discussion and resource centre
adminFirstname - This is the admins first name - used in emails etc.
adminSurname - This is the admins surname
curDomain - Current domain To allow absolute paths in emails etc.
adminEmail - the default admin email.
