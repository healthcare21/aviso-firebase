// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDyNRUk5qPRyud2_nDG-C_oNyFov0fS9lo",
    authDomain: "virtuo-d3fba.firebaseapp.com",
    databaseURL: "https://virtuo-d3fba.firebaseio.com",
    projectId: "virtuo-d3fba",
    storageBucket: "virtuo-d3fba.appspot.com",
    messagingSenderId: "15580895043"
  }
};
