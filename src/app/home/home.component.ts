﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { Globals } from '../globals'
import { EventsService, UserService } from '../_services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  moduleId: module.id,
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {
  currentUser: any;
  users: any;
  eventDetails: any;
  webexurl: any;
  basesixfour: any;
  totaldiscussions: any;

  constructor( public globals: Globals, private EventsService: EventsService, private UserService: UserService, private modalService: NgbModal, private _sanitizer: DomSanitizer, private http: HttpModule, private renderer: Renderer2) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.renderer.removeClass(document.body, 'shortbackground');
    if (this.currentUser.avatar === undefined || !this.currentUser.avatar) {
      this.basesixfour = 'null';
    } else {
      this.basesixfour = this.currentUser.avatar.value
    }
    this.loadAllUsers();
    //get number of discussions
    this.EventsService.countDiscussions().subscribe(singleusers => { 
      this.totaldiscussions = singleusers.docs.length;
    })
  }

  private loadAllUsers() {
    this.UserService.getAllFire().subscribe(users => { 

      let allUsers = [];
      for (var i = 0; i < users.length; i++) {
        allUsers.push(users[i].payload.doc.data());
        allUsers[i].id = users[i].payload.doc.id;
      }
      this.users = allUsers;

     });
  }


  openEdit(reply, content) {

    this.modalService.open(content).result.then((result) => {
    });
  }



  //Check that the user is currently logged into the event
  checkevent(data) {
    if (data.eventid == this.globals.event) {
      //alert('registered');
    }
  }

}
