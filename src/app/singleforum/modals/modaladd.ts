import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import * as cleanhtml from '../../singleforum/cleanhtml';
import { NgForm } from '@angular/forms';  
import { UserService, ForumsService, AlertService } from '../../_services/index';
import * as functions from '../../singleforum/atfunctions'
import { Globals } from '../../globals'

@Component({
  selector: 'app-modaladd',
  templateUrl: './modal.modal.html',
  styleUrls: ['./modal.modal.scss']
})
export class ModalComponent implements OnInit {
  @Input() currentUser: any; 
  dateVar :any;
  myForm: any;
  id: any; sub: any;
  authorRef: any;
  authorRefr: any;
  mentionedDocotrs: any;
  closeResult: string;
  modalReference: any;
  keyupVal: any; keyUpValueMaintian: any; isReadOnly: boolean;
  textareaPosition: any; replyInput: any; numberofats: any; users: any; totalUsers: any; searchValue: any;

  constructor( public globals: Globals, private UserService: UserService, private route: ActivatedRoute, private modalService: NgbModal, private ForumService: ForumsService, private AlertService: AlertService, private _fb: FormBuilder) { }

  ngOnInit() {
      this.numberofats = 0; this.mentionedDocotrs = [];
    this.dateVar = new Date();  this.keyupVal = '';  this.textareaPosition = 0;
    this.sub = this.route.params.subscribe(params => {
        this.id = params['id'];
        //  this.fn();
            // alert('ID!! '+this.id);
        // this.createForm()
    })
    this.loadAllUsers();
  }
  private loadAllUsers() {
    this.UserService.getAllFire().subscribe(users => { 
      let allUsers = [];
      for (var i = 0; i < users.length; i++) {
        allUsers.push(users[i].payload.doc.data());
        allUsers[i].id = users[i].payload.doc.id;
      }
      this.users = allUsers;

     });
      // this.totalUsers = this.users.length; this.arrayUserNames(users); });
  }

  private arrayUserNames(data) {
    this.searchValue = [];
    for (var i = 0; i < data.length; i++) {
      this.searchValue.push(data[i].firstName + ' ' + data[i].lastName);
    }
  }
  getName(authoref, event, reply, el) {
    this.textareaPosition = authoref.selectionStart;
    var keyActual = event.key; //All this so as event.event.key triggers with the Shift as well
    var count = reply.value.split("@").length - 1;
    if (keyActual == 'Backspace') {
      this.numberofats = count;
    }
    if (count > this.numberofats) {
      this.numberofats++;
      this.keyupVal = '@';
      this.isReadOnly = true;
      el.blur();
    }
  }

  setPosition(reply) {
    this.textareaPosition = reply.selectionStart;
  }

  addNametoReply(usersName, userId, reply) {
    var returnText = ''; var x = 0; var addMentioned = true;
    var doctors = {};
    doctors['name'] = usersName;
    doctors['id'] = userId;
    for (var i = 0; i < this.mentionedDocotrs.length; i++) {
      if (this.mentionedDocotrs[i].name == usersName) {
        addMentioned = false;
      }
    }
    if (addMentioned) {
      this.mentionedDocotrs.push(doctors);
    }
    this.authorRef = returnText;
    this.keyUpValueMaintian = ''; this.keyupVal = '';//CLose popup
    this.isReadOnly = false;
    if (this.textareaPosition == 0) {
      this.replyInput = this.replyInput + usersName;
    } else { //Add Doctors name to the current setPosition
      this.replyInput = this.replyInput.substr(0, this.textareaPosition) + usersName + ' ' + this.replyInput.substr(this.textareaPosition);

    }
    reply.focus();
    this.buildAuthoref(reply);
  }


  createForm() {
    this.myForm = this._fb.group({
      _id: '',
      date: '',
      eventid: '',
      name: '',
      question: '',
      username: '',
      replies: this._fb.array([
      ])
    })
  }

  filterCatalogues(authoref, event, reply) {
    var keyActual = event.key; //All this so as event.event.key triggers with the Shift as well
    if (event.key == 'Backspace') {
      var externalReturn = functions.removeSpansExternal(authoref, this.mentionedDocotrs, this.keyUpValueMaintian);
      if (externalReturn[1] == 'none') {
        this.keyupVal = '';
        this.isReadOnly = false;
        var externalAuthoref = functions.buildAuthoref(authoref, this.mentionedDocotrs);
        if (reply != 'authorRefr') { this.authorRef = externalAuthoref; } else { this.authorRefr = externalAuthoref }
      }
      this.mentionedDocotrs = externalReturn[0]; this.keyUpValueMaintian = externalReturn[2];
    } else {
      if (event.key.length < 2) {
        if (keyActual != '@' && this.keyupVal != '@') {
          if (keyActual != '2' && this.keyupVal != '2') {
            alert('Please start your search with the @ symbol' + keyActual);
            if (reply != 'authorRefr') {
              this.authorRef = this.authorRef.substring(0, this.authorRef.length - 1);
            } else {
              this.authorRefr = this.authorRefr.substring(0, this.authorRefr.length - 1);
            }
          }
        }
        if (this.keyupVal == '@' || this.keyupVal == '2') {
          this.keyUpValueMaintian = this.keyUpValueMaintian + keyActual;
        } else {
          if (keyActual == '@' || keyActual == '2') {
            this.keyupVal = '@';
          }
        }
      }
    }
  }
  setkeyupVal(el) {
    this.keyupVal = '';
    this.isReadOnly = false;
    el.focus();
  }

  buildAuthoref(reply) {
    this.authorRef = ''; this.authorRefr = '';
    for (var i = 0; i < this.mentionedDocotrs.length; i++) {
      if (i > 0) { this.authorRef = this.authorRef + ','; }
      if (reply != 'authorRefr') {
        this.authorRef = this.authorRef + '@' + this.mentionedDocotrs[i].name;
      } else {
        this.authorRefr = this.authorRefr + '@' + this.mentionedDocotrs[i].name;
      }
    }
  }

  updateForm(form : NgForm) {
    //CHECK THAT THE MENTIONED DOCTORS ARE STILL IN THE TEXT
    // const controlReply = <FormArray>this.myForm.controls;
     var cleanhtmlreturn = cleanhtml.cleanPaste(this.replyInput); //Clean up - probably not neccessary on plain text input.
    // if (this.mentionedDocotrs.length) { //Email mentioned doctors - if there exists an array of doctors
    //   for (var i = 0; i < this.mentionedDocotrs.length; i++) {//Set defaults for emailing.
    //     for (var j = 0; j < this.users.length; j++) {
    //       if (this.mentionedDocotrs[i].id == this.users[j]._id) {
    //         //  console.log(this.mentionedDocotrs[i]);
    //         var n = cleanhtmlreturn.includes(this.mentionedDocotrs[i].name);
    //         if (n) { //Check that the doctors are still in the text and the user hasn't deleted it
    //           var cleanhtmlreturn = cleanhtmlreturn.replace(this.mentionedDocotrs[i].name, "<i>" + this.mentionedDocotrs[i].name + "</i>");
    //           //alert('Name: ' + this.mentionedDocotrs[i].name + 'is still in the text');
    //           this.EmailServiceService.sendemail(this.users[j].email, this.users[j].firstName, this.users[j].lastName)
    //         }
    //       }
    //     }
    //   }
    // }
    // if (authorref.value) {    //Send email to author with authorref
    //   for (var i = 0; i < this.users.length; i++) {//Set defaults for emailing.
    //     if (this.users[i].firstName + ' ' + this.users[i].lastName == authorref.value) {
    //       //this.EmailServiceService.sendemail(this.users[i].email, this.users[i].firstName, this.users[i].lastName)
    //     }
    //   }
    // }
    // if (!this.eventDetails.inmoderation) { var inmoderation = 'false'; } else { var inmoderation = 'true'; }
    // for (let i = 0; i < this.users.length; i++) {
    //   if (!this.users[i].optOut) { //filter by opt out.
    //     //this.EmailServiceService.sendemailReply(this.users[i].email, this.forums._id, this.forums.name, this.users[i]._id);
    //   }
    // }
     let forumObj = {};
    this.dateVar = new Date();
    forumObj['reply'] = cleanhtmlreturn;
   
     forumObj['username'] = form.value.username;
     forumObj['authorref'] = form.value.userid;
     forumObj['date'] = this.dateVar;
     forumObj['forumId'] = this.id;
     forumObj['inmoderation'] = this.globals.moderation;
    // forumObj['forumlabel'] = this.forumshortTitle;
     forumObj['replytoreply'] = false
     
    this.ForumService.createreplyFirem(forumObj)
      //this.getForums();

    //   if (!this.eventDetails.inmoderation) {
    //     this.AlertService.success('Update successful', true);
    //   } else {
    //     this.AlertService.success('Update successful - awaiting moderation', true);
    //   }
    this.replyInput = '';
    //   this.toggleform();
    this.modalReference.close();
  }

  openEdit(content){
    this.modalReference = this.modalService.open(content)
    this.modalReference.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
