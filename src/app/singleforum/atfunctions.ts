//Functions to handle the @ event and pass back relative data to component.

//Function to strip text between spans.
export function removeSpansExternal(authoref, mentionedDocotrs, keyUpValueMaintian) {
  var numberofAts = authoref.value.split('@').length - 1; //test for nujmber of @'s
  if (numberofAts == mentionedDocotrs.length) {
    mentionedDocotrs.splice(-1, 1)
    var keyupVal = 'none';
  } else {
    keyUpValueMaintian = keyUpValueMaintian.substring(0, keyUpValueMaintian.length - 1);
  }
  return [mentionedDocotrs, keyupVal, keyUpValueMaintian]
}


export function buildAuthoref(reply, mentionedDocotrs) {
  var authorRef = ''; var authorRefr = '';
  for (var i = 0; i < mentionedDocotrs.length; i++) {
    if (i > 0) { authorRef = authorRef + ','; }
    if (i > 0) { authorRefr = authorRefr + ','; }
    if (reply != 'authorRefr') {
      authorRef = authorRef + '@' + mentionedDocotrs[i].name;
      return authorRef;
    } else {
      authorRefr = authorRefr + '@' + mentionedDocotrs[i].name;
      return authorRefr;
    }
  }
}
