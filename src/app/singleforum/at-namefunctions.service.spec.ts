import { TestBed, inject } from '@angular/core/testing';

import { AtNamefunctionsService } from './at-namefunctions.service';

describe('AtNamefunctionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AtNamefunctionsService]
    });
  });

  it('should be created', inject([AtNamefunctionsService], (service: AtNamefunctionsService) => {
    expect(service).toBeTruthy();
  }));
});
