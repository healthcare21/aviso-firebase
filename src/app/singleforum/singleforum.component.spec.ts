import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleforumComponent } from './singleforum.component';

describe('SingleforumComponent', () => {
  let component: SingleforumComponent;
  let fixture: ComponentFixture<SingleforumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleforumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleforumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
