import { Injectable } from '@angular/core';
import { UserService } from '../_services/index';
import { Globals } from '../globals'

@Injectable()
export class EmailServiceService {

  constructor(private UserService: UserService, public globals: Globals) { }

  sendemailReply(email, forumid, topic, userid) {
    var bodyPP = {
      "to": email,
      "body": "Dear Doctor, <br/>A new reply has been posted on the " + this.globals.sitename + "<br/><br/>The reply is to <b>" + topic + "</b> You can view the reply <a href='" + this.globals.curDomain + '/forum/' + forumid + "'>here</a><br/>",
      "subject": this.globals.sitename + " - New reply"
    };
    this.UserService.sendemail(bodyPP).subscribe(event => { console.log(event); });
  }

  sendemailadmin(email, firstname, lastName, currentUser, forumid, replyreplytext) {
    var bodyPP = {
      "to": email,
      "body": "Dear " + firstname + " " + lastName + ": <br/>" + currentUser.firstName + ',' + currentUser.lastName + ' has added a comment with a targeted keyword or targeted keywords please visit: <a href="' + this.globals.curDomain + 'forum/' + forumid + '">' + forumid + '</a><br/><br/><br/><b>Reply text:</b><br/>' + replyreplytext,
      "subject": "Virtual Advisory Board - targeted keyword"
    };
    this.UserService.sendemail(bodyPP).subscribe(event => { });
  }

  sendemailadminRegister(email, firstname, lastName, firstnamer, surname) {
    // var bodyPP = {
    //   "to": email,
    //   "body": "Dear " + firstname + " " + lastName + ": <br/>" + firstnamer + ',' + surname + ' has recently registered',
    //   "subject": "Virtual Advisory Board - new user"
    // };
    // this.UserService.sendemail(bodyPP).subscribe(event => { });
  }


  sendemail(email, firstname, lastName) {
    var bodyPP = {
      "to": email,
      "body": "Dear " + firstname + " " + lastName + ": <br/><br/>Somebody has mentioned  you  in a comment, please visit the Virtual Adisory Board at: " + this.globals.curDomain + "/forum",
      "subject": "Virtual Advisory Board - comments"
    };
    this.UserService.sendemail(bodyPP).subscribe(event => { console.log(event); });
  }

}
