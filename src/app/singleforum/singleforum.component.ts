import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, EventsService, ForumsService, AlertService } from '../_services/index';
import { Globals } from '../globals'
import { FormBuilder, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ModrequiresPipe } from '../pipes/modrequires.pipe';
import { OrderPipePipe } from '../pipes/order-pipe.pipe';
import { FilterbyAuthorPipe } from '../pipes/filterby-author.pipe';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { EmailServiceService } from '../singleforum/email-service.service';
import * as functions from '../singleforum/atfunctions'
import * as cleanhtml from '../singleforum/cleanhtml'
import { Location } from '@angular/common';
import * as _ from "lodash";

@Component({
  selector: 'app-singleforum',
  templateUrl: './singleforum.component.html',
  styleUrls: ['./singleforum.component.scss'],
  providers: [ModrequiresPipe, OrderPipePipe, FilterbyAuthorPipe,]
})
export class SingleforumComponent implements OnInit {
  forums: any; webexurl: any; eventDetails: any; myForm: any; currentUser: any; sub: any; id: any; showForm: any; showExpand: any;
  dateVar: any; buttonText: any; expandText: any; users: any; currentPage: any; paginationArr: any; haspagination: any; paginationlinks: any;
  forumModerated: any; showreplyreply: any; totalUsers: any; showBananas: boolean; sortorder: any; closeResult: string; replyId: string; ismoderatedRR: string;
  modaltTitle: string; replyreplyText: any; replyText: any; allforumsSing: any; uniqueId: any; currentAuthor: any; searchValue: any; searchreturn: any;
  forumsubscribe: any; emailsubscribe: any; forumcount: any;
  editreply: any; showEdit: any; editForumValue: any; likes: any; authorRefr: any;
  buttonText2: any; keyupVal: any; mentionedDocotrs: any; authorRef: any; keyUpValueMaintian: any; //Not tired, not tired can think, honest.
  allReplies: any; replyInput: any; textareaPosition: any; replyAuthors: any; forumshortTitle: string; numberofats: any; //log number of ats in the text area
  showUpload: any;
  uniqueusers: any;
  current: any;

  replyservice: any;
  isReadOnly: boolean;
  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.searchValue.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

  constructor(private location: Location, private EmailServiceService: EmailServiceService, private FilterbyAuthorPipe: FilterbyAuthorPipe, private modalService: NgbModal, private modrequires: ModrequiresPipe, private OrderPipePipe: OrderPipePipe, private UserService: UserService, private ForumService: ForumsService, private _fb: FormBuilder, public globals: Globals, private EventsService: EventsService, private _sanitizer: DomSanitizer, private AlertService: AlertService, public router: Router, private route: ActivatedRoute) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.current = [];
    router.events.subscribe((val) => {
      this.haspagination = false;
      this.forums = [];
    });
  }


  ngOnInit() {
    this.numberofats = 0; this.isReadOnly = false;
    this.keyupVal = ''; this.mentionedDocotrs = []; this.keyUpValueMaintian = ''; this.showEdit = 'false'; this.textareaPosition = 0;
    var sortorderVar = localStorage.getItem('sortorder');
    if (sortorderVar) { this.sortorder = sortorderVar; } else { this.sortorder = 'default'; }
    this.forumModerated = []; this.currentPage = 1; this.haspagination = false; this.showForm = false; this.showExpand = false;
    this.sub = this.route.params.subscribe(params => {
      if (this.forumsubscribe) { this.forumsubscribe.unsubscribe(); }
      if (this.replyservice) { this.replyservice.unsubscribe(); }
      this.id = params['id']; // (+) converts string 'id' to a number
       this.getEvent(); 
      this.getForums(); 
      this.loadAllUsers();
    });
    this.dateVar = new Date(); this.buttonText = 'Post a response'; this.buttonText2 = 'Add comment'; this.expandText = 'Show topic information'; this.showreplyreply = 1000000; this.currentAuthor = 'none';
  }

  addNametoReply(usersName, userId, reply) {
    var returnText = ''; var x = 0; var addMentioned = true;
    var doctors = {};
    doctors['name'] = usersName;
    doctors['id'] = userId;
    for (var i = 0; i < this.mentionedDocotrs.length; i++) {
      if (this.mentionedDocotrs[i].name == usersName) {
        addMentioned = false;
      }
    }
    if (addMentioned) {
      this.mentionedDocotrs.push(doctors);
    }
    this.authorRef = returnText;
    this.keyUpValueMaintian = ''; this.keyupVal = '';//CLose popup
    this.isReadOnly = false;
    if (this.textareaPosition == 0) {
      this.replyInput = this.replyInput + usersName;
    } else { //Add Doctors name to the current setPosition
      this.replyInput = this.replyInput.substr(0, this.textareaPosition) + usersName + ' ' + this.replyInput.substr(this.textareaPosition);

    }
    reply.focus();
    this.buildAuthoref(reply);
  }



  buildAuthoref(reply) {
    this.authorRef = ''; this.authorRefr = '';
    for (var i = 0; i < this.mentionedDocotrs.length; i++) {
      if (i > 0) { this.authorRef = this.authorRef + ','; }
      if (reply != 'authorRefr') {
        this.authorRef = this.authorRef + '@' + this.mentionedDocotrs[i].name;
      } else {
        this.authorRefr = this.authorRefr + '@' + this.mentionedDocotrs[i].name;
      }
    }
  }

  addNametoReplyreply(usersName, userId) {
    var returnText = '';
    for (var i = 0; i < this.replyreplyText.length; i++) {
      if (this.replyreplyText[i] == '@') {
        returnText = returnText + '<p class="doctor">@' + usersName + '</p> ';
      } else {
        returnText = returnText + this.replyreplyText[i];
      }
    }
    this.mentionedDocotrs.push(userId);
    this.replyreplyText = returnText;
    this.keyUpValueMaintian = ''; this.keyupVal = '';//CLose popup
  }
  setPosition(reply) {
    this.textareaPosition = reply.selectionStart;
  }


  getName(authoref, event, reply, el) {

    this.textareaPosition = authoref.selectionStart;
    var keyActual = event.key; //All this so as event.event.key triggers with the Shift as well
    var count = reply.value.split("@").length - 1;
    if (keyActual == 'Backspace') {
      this.numberofats = count;
    }
    if (count > this.numberofats) {
      this.numberofats++;
      this.keyupVal = '@';
      this.isReadOnly = true;
      el.blur();
    }
  }
  filterCatalogues(authoref, event, reply) {
    var keyActual = event.key; //All this so as event.event.key triggers with the Shift as well
    if (event.key == 'Backspace') {
      var externalReturn = functions.removeSpansExternal(authoref, this.mentionedDocotrs, this.keyUpValueMaintian);
      if (externalReturn[1] == 'none') {
        this.keyupVal = '';
        this.isReadOnly = false;
        var externalAuthoref = functions.buildAuthoref(authoref, this.mentionedDocotrs);
        if (reply != 'authorRefr') { this.authorRef = externalAuthoref; } else { this.authorRefr = externalAuthoref }
      }
      this.mentionedDocotrs = externalReturn[0]; this.keyUpValueMaintian = externalReturn[2];
    } else {
      if (event.key.length < 2) {
        if (keyActual != '@' && this.keyupVal != '@') {
          if (keyActual != '2' && this.keyupVal != '2') {
            alert('Please start your search with the @ symbol' + keyActual);
            if (reply != 'authorRefr') {
              this.authorRef = this.authorRef.substring(0, this.authorRef.length - 1);
            } else {
              this.authorRefr = this.authorRefr.substring(0, this.authorRefr.length - 1);
            }
          }
        }
        if (this.keyupVal == '@' || this.keyupVal == '2') {
          this.keyUpValueMaintian = this.keyUpValueMaintian + keyActual;
        } else {
          if (keyActual == '@' || keyActual == '2') {
            this.keyupVal = '@';
          }
        }
      }
    }
  }
  setkeyupVal(el) {
    this.keyupVal = '';
    this.isReadOnly = false;
    el.focus();
  }
  //Modal HttpClientModule
  open(content, replyId, username) {
    this.mentionedDocotrs = []; this.showForm = false;
    this.showEdit = 'false';
    this.replyId = replyId; this.modaltTitle = username;
    this.modalService.open(content).result.then((result) => {
    });
  }

  openEdit(reply, content) {
    console.log('reply',reply);
    //Replace reply text <i> and </i>
    this.replyText = reply.reply;
    var re = new RegExp('<i>', 'g');
    this.replyText = this.replyText.replace(re, "");
    var re = new RegExp('</i>', 'g');
    this.replyText = this.replyText.replace(re, "");
    this.replyAuthors = reply.authorref;
    this.replyId = reply.id;
    this.editreply = reply.uniqueId;
    this.showEdit = 'reply';
    this.editForumValue = reply;
    this.ismoderatedRR = reply.inmoderation;
    this.modalService.open(content).result.then((result) => {
    });
  }


  

  openEditrr(reply, content) {
    this.replyText = reply.reply;
    this.editreply = reply.uniqueId;
    this.showEdit = 'replyreply';
    this.editForumValue = reply;
    this.modalService.open(content).result.then((result) => {
    });
  }

  updateFormReply(username, title, reply, authorref, userid, uniqueId, date, _id, ismoderated) {
    var cleanhtmlreturn = cleanhtml.cleanPaste(reply.value);
    var doctors = authorref.value.split(",");
    for (var i = 0; i < doctors.length; i++) {
      var cleanhtmlreturn = cleanhtmlreturn.replace(doctors[i], "<i>" + doctors[i] + "</i>");
    }
    //  var cleanhtmlreturn = cleanhtmlreturn.replace(this.mentionedDocotrs[i].name, "<i>" + this.mentionedDocotrs[i].name + "</i>");

    let forumObj = {};
    this.dateVar = new Date();
    forumObj['reply'] = cleanhtmlreturn;
    forumObj['username'] = username.value;
    forumObj['authorref'] = authorref.value;
    forumObj['date'] = this.dateVar;
    forumObj['forumId'] = this.id;
    forumObj['forumlabel'] = this.forumshortTitle;
    forumObj['replytoreply'] = false;
    forumObj['inmoderation'] = ismoderated.value;
    // console.log('forumObj', forumObj, '_id', _id);

    this.ForumService.updatereplyFirem(forumObj, _id)
      //this.getForums();

      if (!this.eventDetails.inmoderation) {
        this.AlertService.success('Update successful', true);
      } else {
        this.AlertService.success('Update successful - awaiting moderation', true);
      }
      this.replyInput = '';
      // this.toggleform();
  }

  unlikeMe(uniqueId, currentUserId) {
    for (var i = 0; i < this.forums.likes.length; i++) {
      if (this.forums.likes[i].replyId == uniqueId && this.forums.likes[i].userid == currentUserId) {
        //remove at i as index
        const patchV = (<FormArray>this.myForm.controls['likes']) as FormArray;
        patchV.removeAt(i);
      }
    }
    this.forumsubscribe.unsubscribe();
    this.ForumService.update(this.myForm.value).subscribe(singleusers => { this.replyreplyText = ''; this.getForums(); });
  }

  trackByFn(index, item) {
    return index; // or item.id
  }

  updateFormReplyreply(username, title, reply, authorref, userid, uniqueId) {
    for (var i = 0; i < this.forums.replies.length; i++) {
      var replyreply = this.forums.replies[i].replies;
      if (replyreply.length) {
        for (var j = 0; j < replyreply.length; j++) {
          if (replyreply[j].uniqueId == uniqueId) {
            const patchV = (<FormArray>this.myForm.controls['replies']).at(i) as FormArray;
            var patchReplies = (<FormArray>patchV.controls['replies']).at(j) as FormArray;
            patchReplies.controls['reply'].patchValue(this.replyText);
            this.ForumService.update(this.myForm.value).subscribe(singleusers => {
              this.AlertService.success('Update successful', true);
            })
          }
        }
      }
    }
  }


  private toggleforumorder() { //Toggles forum order resets replies and order in this.pagination
    if (this.sortorder == 'default') {
      localStorage.setItem('sortorder', 'earliest');
      this.sortorder = "earliest";
    } else {
      this.sortorder = 'default';
      localStorage.setItem('sortorder', 'default');
    }
      this.ForumService.getRepliesByID(this.id).subscribe (replies => { 
      this.allReplies = replies; this.pagination(replies);
     });
  }

  @HostListener("window:scroll", []) //Scroll test for fixed popup
  onWindowScroll() {
    if (window.pageYOffset < 422) {
      if (this.showBananas) {
        this.showBananas = false;
      }
    }
    if (window.pageYOffset >= 422) {
      if (!this.showBananas) {
        this.showBananas = true;
      }
    }
  }

  setReplyform(index, replyreplyText) {
    this.showreplyreply = index;
  }

  toggleExpand() {
    this.showExpand = !this.showExpand;
    if (this.expandText == 'Show topic information') {
      this.expandText = 'Hide';
    } else {
      this.expandText = 'Show topic information';
    }
  }

  toggleform() {
    this.mentionedDocotrs = [];
    this.showForm = !this.showForm;
    this.numberofats = 0;
    this.replyText = '';
    if (this.buttonText == 'Post a response' || this.buttonText2 == 'Post a response') {
      this.buttonText = 'Hide';
      this.buttonText2 = 'Hide';
      window.scrollTo({
        top: 200,
        behavior: "smooth"
      });
    } else {
      this.buttonText = 'Post a response';
      this.buttonText2 = 'Post a response';
      this.buttonText2 == 'Post a response'
    }
  }

  private getForums() {

    this.ForumService.getSinglefire(this.id).subscribe (forums => { 
      this.forums = forums; this.likes = forums['likes']; this.setInitialForm(forums); this.forumshortTitle = forums['lable'];
     
    });
    if(this.globals.moderation === 'moderated'){
    this.ForumService.getRepliesByID(this.id).subscribe (replies => { 
      this.allReplies = replies; this.pagination(replies);
      this.populateForumusers(replies);
     });
    } else {
      this.ForumService.getRepliesByIDnoMod(this.id).subscribe (replies => { 
        this.allReplies = replies; this.pagination(replies);
        this.populateForumusers(replies);
       });
      
    }
    // this.forumsubscribe = this.ForumService.getSingle(this.id).subscribe(forums => { this.forums = forums; this.likes = forums['likes']; this.setInitialForm(forums); this.forumshortTitle = forums['lable'] });
    // this.forumcount = this.ForumService.getAllforumIDS().subscribe(forumid => { this.countAll(forumid); });
    // this.replyservice = this.ForumService.getSinglereply(this.id).subscribe(replies => { this.allReplies = replies; this.pagination(replies); });
  }
  private getEvent() {
    this.EventsService.getSingleFire(this.globals.event).subscribe (event => { 
      this.eventDetails = event;
      this.webexurl = this._sanitizer.bypassSecurityTrustResourceUrl(this.eventDetails.webex);
    });
  }

  private populateForumusers(forums) {
    let allUsers = [];
    for (var i = 0; i < forums.length; i++) {
     
      allUsers.push(forums[i].payload.doc.data().username);
    }
    this.uniqueusers = _.uniq(allUsers);
     
  }

  private loadAllUsers() {
    this.UserService.getAllFire().subscribe(users => { 

      let allUsers = [];
      for (var i = 0; i < users.length; i++) {
        allUsers.push(users[i].payload.doc.data());
        allUsers[i].id = users[i].payload.doc.id;
      }
      this.users = allUsers;


      this.totalUsers = this.users.length; this.arrayUserNames(users); });
  }

  private arrayUserNames(data) {
    this.searchValue = [];
    for (var i = 0; i < data.length; i++) {
      this.searchValue.push(data[i].firstName + ' ' + data[i].lastName);
    }
  }

  private countAll(allforums) { //Loop around all forum posts and get the last uniqueID
    let x = 1;
    for (var i = 0; i < allforums.length; i++) {
      if (allforums[i].id) {
        if (parseInt(allforums[i].id) > x) {
          x = parseInt(allforums[i].id);
        }
      }
    }
    this.uniqueId = x;
  }

  private onOptionsSelected(event) {
    this.currentAuthor = event;
    // this.forumsubscribe.unsubscribe(
      this.ForumService.getRepliesByID(this.id).subscribe(replies => {
        this.allReplies = replies; 
        this.pagination(replies);
        // this.pagination(replies);
      })
    //);
  }


  setPage(index) {
    this.currentPage = index;
  }
  showallReplies() {
    this.haspagination = !this.haspagination;
  }

  objectsize(obj) {
    var size = 0, key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  };
  private replytoreplyr(replyIndex, reply, username, title, authorref, userid) {   //Get latest data in case it has changed
    //Compare reply to wordlist and email admin if word is in th reply.
    this.mentionedDocotrs = [];
    for (var i = 0; i < this.eventDetails.wordlist.length; i++) {
      if (this.replyreplyText.includes(this.eventDetails.wordlist[i].wordlistName)) {
        var sendadminEmail = true;
      } else {
        sendadminEmail = false;
      }
      if (sendadminEmail) {
        this.EmailServiceService.sendemailadmin(this.globals.adminEmail, this.globals.adminFirstname, this.globals.adminSurname, this.currentUser, this.id, this.replyreplyText)
        sendadminEmail = false;
      }
    }
    if (this.mentionedDocotrs.length) {
      for (var i = 0; i < this.mentionedDocotrs.length; i++) {//Set defaults for emailing.
        for (var j = 0; j < this.users.length; j++) {//Set defaults for emailing.
          if (this.mentionedDocotrs[i] == this.users[j]._id) {
            this.EmailServiceService.sendemail(this.users[j].email, this.users[j].firstName, this.users[j].lastName)

          }
        }
      }
    }
    if (authorref.value) {
      //Set defaults for emailing.
      for (var i = 0; i < this.users.length; i++) {
        if (this.users[i].firstName + ' ' + this.users[i].lastName == authorref.value) {
          this.EmailServiceService.sendemail(this.users[i].email, this.users[i].firstName, this.users[i].lastName)
        }
      }
    }
    //for (let i = 0; i < this.users.length; i++) {
    // if (!this.users[i].optOut) { //filter by opt out.
    //   if (this.users[i].email != this.currentUser.email) {
    //     this.EmailServiceService.sendemailReply(this.users[i].email, this.forums._id, this.forums.name);
    //   }
    // } Removed reply reminder for all posts
    //}
    if (this.globals.moderation === 'on') { var inmoderation = 'true'; } else { var inmoderation = 'false'; }

    var cleanhtmlreturn = cleanhtml.cleanPaste(this.replyreplyText);

    var controlReply = this.myForm.controls['replies'];
    controlReply = controlReply.at(replyIndex);
    controlReply = controlReply.controls['replies'];
    this.forumcount = this.ForumService.getAllforumIDS().subscribe(forumid => {
      var length = this.objectsize(forumid);
      var unique = parseInt(forumid[length - 1].id) + 1;
      controlReply.push(
        this._fb.group({
          name: title.value,
          reply: cleanhtmlreturn,
          username: username.value,
          date: this.dateVar,
          uniqueId: unique,
          inmoderation: inmoderation,
          authorref: authorref.value,
          userid: userid.value
        })
      );
      this.forumsubscribe.unsubscribe(); //Stop form propagating and updating
      this.ForumService.update(this.myForm.value).subscribe(singleusers => {
        this.replyreplyText = '';
        this.ForumService.createid(unique).subscribe(forumid => { });
        if (!this.eventDetails.inmoderation) {
          this.AlertService.success('Update successful', true);
          this.replyreplyText = '';
        } else {
          this.AlertService.success('Update successful - awaiting moderation', true);
        }
        this.getForums();
        //this.router.navigate(['/forum']);

      })
    });
  }

  pagination(forum) {
    //based on 5 per page
    let forumPass = []; let index =0;
    forum.map(e => {
      forumPass.push(e.payload.doc.data())
      forumPass[index]['id'] = e.payload.doc.id
      index ++;
    })

    this.forumModerated = forumPass;
    this.forumModerated = this.FilterbyAuthorPipe.transform(this.forumModerated, this.currentAuthor);
    if (this.sortorder == 'default') {
      this.forumModerated = this.OrderPipePipe.transform(this.forumModerated, 'default');
    } else {
      this.forumModerated = this.OrderPipePipe.transform(this.forumModerated, '11');
    }
  
    if (this.forumModerated.length > 5) {
      this.paginationArr = [];
      this.haspagination = true;
      this.paginationlinks = Math.ceil(this.forumModerated.length / 5);
      let i = 1;
      while (i <= this.paginationlinks) {
        this.paginationArr.push(i);
        i++;
      }
    } else {
      this.haspagination = false;
    }
  }

  createForm() {
    this.myForm = this._fb.group({
      _id: '',
      date: '',
      eventid: '',
      name: '',
      question: '',
      username: '',
      replies: this._fb.array([
      ])
    })
  }
  likeME(currentId) {
    var control = <FormArray>this.myForm.controls['likes'];
    control.push(
      this._fb.group({
        replyId: currentId,
        userid: this.currentUser._id
      })
    )
    this.forumsubscribe.unsubscribe();
    this.ForumService.update(this.myForm.value).subscribe(singleusers => { this.replyreplyText = ''; this.getForums(); });
  }
  setInitialForm(data) {
    this.myForm = this._fb.group({
      _id: data._id,
      date: data.date,
      eventid: data.eventid,
      name: data.name,
      question: data.question,
      username: data.username,
      likes: this._fb.array([
      ]),
      replies: this._fb.array([
      ])
    });
    var controllikes = <FormArray>this.myForm.controls['likes'];
    if (data.likes) {
      for (var j = 0; j < data.likes.length; j++) {
        controllikes.push(
          this._fb.group({
            replyId: data.likes[j].replyId,
            userid: data.likes[j].userid
          })
        )
      }
    }
    var control = <FormArray>this.myForm.controls['replies'];
    for (var i = 0; i < data.replies.length; i++) {
      control.push(
        this._fb.group({
          name: data.replies[i].name,
          reply: data.replies[i].reply,
          username: data.replies[i].username,
          date: data.replies[i].date,
          inmoderation: data.replies[i].inmoderation,
          uniqueId: data.replies[i].uniqueId,
          authorref: data.replies[i].authorref,
          references: data.replies[i].references,
          userid: data.replies[i].userid,
          replies: this._fb.array([
          ])
        })
      )
      this.addreplyReply(data, i);
    }
  }

  addreplyReply(data, i) {
    var controlReply = this.myForm.controls['replies'];
    controlReply = controlReply.at(i);
    controlReply = controlReply.controls['replies'];
    if (data.replies[i].replies) {
      for (var jj = 0; jj < data.replies[i].replies.length; jj++) {
        controlReply.push(
          this._fb.group({
            name: data.replies[i].replies[jj].name,
            reply: data.replies[i].replies[jj].reply,
            username: data.replies[i].replies[jj].username,
            date: data.replies[i].replies[jj].date,
            uniqueId: data.replies[i].replies[jj].uniqueId,
            inmoderation: data.replies[i].replies[jj].inmoderation,
            authorref: data.replies[i].replies[jj].authorref,
            userid: data.replies[i].replies[jj].userid
          })
        );
      }
    }
  }
  replytoreply(username, name, reply, authorref, userid, titlenew, date) {
    //  console.log(reply.value);

  }
  updateForm(username, name, reply, authorref, userid, titlenew, date, ismoderated) {
    //CHECK THAT THE MENTIONED DOCTORS ARE STILL IN THE TEXT
    const controlReply = <FormArray>this.myForm.controls['replies'];
    var cleanhtmlreturn = cleanhtml.cleanPaste(reply.value); //Clean up - probably not neccessary oin plain text input.
    if (this.mentionedDocotrs.length) { //Email mentioned doctors - if there exists an array of doctors
      for (var i = 0; i < this.mentionedDocotrs.length; i++) {//Set defaults for emailing.
        for (var j = 0; j < this.users.length; j++) {
          if (this.mentionedDocotrs[i].id == this.users[j]._id) {
            //  console.log(this.mentionedDocotrs[i]);
            var n = cleanhtmlreturn.includes(this.mentionedDocotrs[i].name);
            if (n) { //Check that the doctors are still in the text and the user hasn't deleted it
              var cleanhtmlreturn = cleanhtmlreturn.replace(this.mentionedDocotrs[i].name, "<i>" + this.mentionedDocotrs[i].name + "</i>");
              //alert('Name: ' + this.mentionedDocotrs[i].name + 'is still in the text');
              this.EmailServiceService.sendemail(this.users[j].email, this.users[j].firstName, this.users[j].lastName)
            }
          }
        }
      }
    }
    if (authorref.value) {    //Send email to author with authorref
      for (var i = 0; i < this.users.length; i++) {//Set defaults for emailing.
        if (this.users[i].firstName + ' ' + this.users[i].lastName == authorref.value) {
          //this.EmailServiceService.sendemail(this.users[i].email, this.users[i].firstName, this.users[i].lastName)
        }
      }
    }

    for (let i = 0; i < this.users.length; i++) {
      if (!this.users[i].optOut) { //filter by opt out.
        //this.EmailServiceService.sendemailReply(this.users[i].email, this.forums._id, this.forums.name, this.users[i]._id);
      }
    }
    let forumObj = {};
    this.dateVar = new Date();
    forumObj['reply'] = cleanhtmlreturn;
    forumObj['username'] = username.value;
    forumObj['authorref'] = authorref.value;
    forumObj['date'] = this.dateVar;
    forumObj['forumId'] = this.id;
    forumObj['forumlabel'] = this.forumshortTitle;
    forumObj['replytoreply'] = false

    this.ForumService.createreplyFirem(forumObj)
      //this.getForums();

      if (!this.eventDetails.inmoderation) {
        this.AlertService.success('Update successful', true);
      } else {
        this.AlertService.success('Update successful - awaiting moderation', true);
      }
      this.replyInput = '';
      this.toggleform();
  }



	showModal(email, content) {
		//get user details from users
		for (var i = 0; i < this.users.length; i++) {

			if(this.users[i].email === email){
				// console.log(this.users[i].payload.doc.data().email);
				//singleUserdets = this.users[i].payload.doc.data();
				this.current = this.users[i];
			}

		}
		this.modalService.open(content).result.then((result) => {
		});
	}
}
