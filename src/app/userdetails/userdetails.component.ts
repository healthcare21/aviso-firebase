import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Globals} from '../globals'
import { AlertService, UserService, AuthenticationService } from '../_services/index';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.scss']
})
export class UserdetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private UserService: UserService) { }
  users: any;
  sub: any;
  id: any;
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
     this.id = params['id']; // (+) converts string 'id' to a number
     this.loadUser( params['id']);
  })
};

  private loadUser(curId) {
      this.UserService.getUserFire(curId).subscribe(singleusers => { this.users = singleusers.docs[0].data(); });
  }
}
