import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar.component';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { SortdescPipe } from '../pipes/sortdesc.pipe';
import { Routes, RouterModule } from '@angular/router';
import { FilterforumPipe } from '../pipes/filterforum.pipe';
import { DateSortPipe } from '../pipes/date-sort.pipe';
import { LimittextPipe } from '../pipes/limittext.pipe';
import { UploadresComponent } from '../uploadres/uploadres.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { StriphtmlPipe } from '../pipes/striphtml.pipe';



@NgModule({
  declarations: [SidebarComponent,StriphtmlPipe, SortdescPipe, FilterforumPipe, DateSortPipe, LimittextPipe, UploadresComponent],
  imports: [CommonModule, RouterModule, ReactiveFormsModule, FileUploadModule],
  exports: [SidebarComponent, LimittextPipe, UploadresComponent]
})
export class SidebarModule { }
 