import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, ForumsService } from '../_services/index';
import * as _ from "lodash";
import { appConfig } from '../app.config';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() hideSidebar: string; //boolean
  users: any; forums: any; showUpload: boolean; showless: boolean; groupsAll: any;
  basesixfour: any; sub: any; id: any;
  numberUsers: any; totalUsers: any; forumServ: any; forumReply: any;
  allReplies: any; showallreplies: any; numberTopics: any; currentUser: any; userServ: any; singleuser: any; idlink: any;
  uploads:any;     localURL: any;
  current: any;

  constructor(private userService: UserService, private ForumsService: ForumsService, private route: ActivatedRoute,private modalService: NgbModal) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.numberUsers = 4;
    this.showUpload = false;
    this.showless = false;
    this.current = [];
  }

  trackByFn(index, item) {
    return index; // or item.id
  }

  toggleshowUpload(){
    this.showUpload = !this.showUpload;
  }
  showModal(email,content) {
    //get user details from users
    for (var i = 0; i < this.users.length; i++) {
      if(this.users[i].payload.doc.data().email === email){
        this.current = this.users[i].payload.doc.data();
      }
    }
    this.modalService.open(content).result.then((result) => {
    });
  }


  ngOnInit() {
    this.showallreplies = false; this.numberTopics = 3; this.groupsAll = [];
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.localURL = appConfig.apiUrl;
      if(this.id){
        this.idlink = "/addResource/"+this.id;
        //get all resources
        this.ForumsService.getUploadsFire().subscribe(files => { 
          this.uploads = files;
        })
      } 
     })
    this.getallForums(); 
    this.loadAllUsers();
    this.loadAllForums();
    if (this.currentUser.avatar === undefined || !this.currentUser.avatar) {
      this.basesixfour = 'null';
    } else {
      this.basesixfour = this.currentUser.avatar.value
    }
  }
  getallForums() {
   this.ForumsService.getallForumsflat().subscribe(allGroups => {
      for (var i = 0; i < allGroups.docs.length; i++) {
      this.groupsAll.push(allGroups.docs[i].data())
      }

   })
  }
  showmore() {
    this.numberUsers = this.numberUsers + 3;
    //test that this is truley the last item
  //  alert(this.totalUsers+ " :TotalUsers: " + this.numberUsers );
    // if(this.numberUsers >= this.totalUsers){
      this.showless = true;
    // }
   
  }
  showlessFunc() {
    this.numberUsers = this.numberUsers - 3;
    if(this.numberUsers == 4){
      this.showless = false;
    } else {
      this.showless = true;
    }
  }
  showAll() {
    this.showallreplies = !this.showallreplies;
    if (this.showallreplies) {
      this.numberTopics = 10000;
    } else {
      this.numberTopics = 3;
    }
  }
  private loadAllForums() {
    this.userService.getUserFire(this.currentUser.email).subscribe(singleusers => {
      this.singleuser = singleusers.docs[0].data();
      let forumPass =[];
      this.forumServ = this.ForumsService.getallForumsfire().subscribe(forums => { 
        forums.map(e => {
            forumPass.push(e.payload.doc.data())
        })
        this.filterByGroups(forumPass);
       });
   });
   
  }
  private filterByGroups(forums){
    //Get users allowed groups
    let finalArr = []; let allIds = [];
    //Loop around forums and return only allowed forum groups
    for (var k = 0; k < forums.length; k++) {
      let forumarry1 = forums[k].groups;
      let lock = false;
      if(forumarry1){
          if(forumarry1.length == 0){ finalArr.push(forums[k]); lock = true; } //Display forum if it has no groups assigned
          if(this.singleuser.title === 'All'){
            if(!lock) {
              finalArr.push(forums[k]); 
              allIds.push(forums[k]._id);
              lock = true 
             };
          }
          if (!forums[k].groups){
            finalArr.push(forums[k]); //if no groups are set return the forum??   
            allIds.push(forums[k]._id);
            lock = true;
          } else {
              //Check to see if the user has the correct rights
            if(!this.singleuser.group){
              if(!lock){ //No groups set presume all
                finalArr.push(forums[k]);
                allIds.push(forums[k]._id);
                lock = true;
              }
            } else {
              let array1 = this.singleuser.group;
              let array2 = forums[k].groups;
              //Loop around users groups..
              for (var l = 0; l < array1.length; l++) {         
                var dif = _.findIndex(array2, (o) => { return _.isMatch(o, array1[l]) });
                if(dif != -1){
                  if(!lock){
                          finalArr.push(forums[k]);
                          allIds.push(forums[k]._id);
                          lock = true;
                        }
                }
              }
            }
          }
      }
    }
    this.forums = finalArr;
    this.forumReply = this.ForumsService.getAllrepliesFire().subscribe(replies => {
      this.loadAllReplies(replies, allIds);
    })
  }


  private loadAllReplies(replies, allIds) {
   let returnedReplies = [];
   for (var i = 0; i < replies.length; i++) { //Test weather forumId exists on replyId
    let truthBrother = _.includes(allIds, replies[i].forumId)
    if(truthBrother){
       //Get each group name
       for (var j = 0; j < this.groupsAll.length; j++) {
        if(this.groupsAll[j]._id == replies[i].forumId){
          replies[i].forumTitle =  this.groupsAll[j].name; 
        }
      }
    }
    returnedReplies.push(replies[i]);
   }
   this.allReplies = returnedReplies;
   this.UpdateUsersReplies(); //Add Replies to User array
  }

  private UpdateUsersReplies() {
    //Loop around users and get all replies
    let allReps = this.allReplies;
    _.map(this.users, function(user){
      let x = 0;
      var returnCount =  _.map(allReps, function(singleRep){
          if(singleRep.authorref === user.payload.doc.id){
            x = x +1;
          }
      })
      user['count'] = x;
    })
    //organise by top count
    this.users = _.orderBy(this.users, 'count','desc')
  }



  private loadAllUsers() {
    this.userServ = this.userService.getAllFire().subscribe(users => { this.users = users; this.totalUsers = this.users.length;   });
   
  }

  ngOnDestroy() {
    //this.forumServ.unsubscribe();
    this.userServ.unsubscribe();
  }




}
