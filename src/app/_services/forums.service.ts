import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { appConfig } from '../app.config';
import { Forum,Group } from '../_models/index';

import { AngularFirestore } from '@angular/fire/firestore';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/mergeMap'

@Injectable()
export class ForumsService {

  constructor(private http: HttpClient, private firestore: AngularFirestore) { }
  timer = Observable.timer(0, 10000); // Remove obsolete?
  getallForumsfire() {
    let retrieved = this.firestore.collection('forums').snapshotChanges();
    return retrieved
  }

  getallForumsflat() {
    return this.firestore.collection('forums').get();
  }


  createfire(forum: Forum) {
    return this.firestore.collection('forums').add(forum);
  }

  createGroupfire(group: Group){
   // return this.firestore.collection(group._id).doc('groups').set(group);
   return this.firestore.collection('groups').add(group);
  }
  getRepliesByID(forumId){
    return  this.firestore.collection('/replies', ref => ref.where('forumId', '==', forumId)).snapshotChanges();
  }
  getRepliesByIDnoMod(forumId){
    return  this.firestore.collection('/replies', ref => ref.where('forumId', '==', forumId).where('inmoderation', '==', 'moderated')).snapshotChanges();
  }
  acceptModeration(forumId){

    this.firestore.collection('replies').doc(forumId).update({'inmoderation':'moderated'});
    // return this.firestore.collection('forums').doc(forum._id).snapshotChanges();
  }
  getallGroupsfire() {
    return this.firestore.collection('groups').snapshotChanges();
  }

  updateGroupsfire(group: Group, id){
    return this.firestore.collection('groups').doc(id).set(group);
  }

  addFile(filedetails){
    // console.log('Filedetails',filedetails);
    return this.firestore.collection('uploads').add(filedetails);
  }
  getUploadsFire() {
    return this.firestore.collection('uploads').valueChanges();
  }

  updatefire(forum: Forum) {
   this.firestore.collection('forums').doc(forum._id).update(forum);
   return this.firestore.collection('forums').doc(forum._id).snapshotChanges();
  }

  getGroupsFire(){
    return this.firestore.collection('groups').snapshotChanges();
  }
  createreplyFirem(forum) {
    return this.firestore.collection('replies').add(forum);
  }
  updatereplyFirem(forum,_id) {
    // console.log('id', _id);
    return this.firestore.collection('replies').doc(_id).set(forum);
  }

  getSinglefire(id) {
    return this.firestore.collection('forums').doc(id).valueChanges();
  }

  getAllrepliesFire() {
    return this.firestore.collection('replies').valueChanges();
  }

  getAllForums() {
    return this.http.get(appConfig.apiUrl + '/forums/getAllforums');
  }
  // getSingle(id) {
  //   return this.timer
  //     .flatMap((i) => this.http.get(appConfig.apiUrl + '/forums/getSingle/' + id));
  // }
  getSingle(id) {
    return this.http.get(appConfig.apiUrl + '/forums/getSingle/' + id);
  }
  getAllForumsstatic() {
    return this.http.get(appConfig.apiUrl + '/forums/getAllforums');
  }
  getSinglestatic(id) {
    return this.http.get(appConfig.apiUrl + '/forums/getSingle/' + id);
  }
  createid(id) {
    return this.http.get(appConfig.apiUrl + '/forumid/create/' + id);
  }
  getAllforumIDS() {
    return this.http.get(appConfig.apiUrl + '/forumid/getAllforumIDS');
  }
  getAllreplies() {
    return this.timer
      .flatMap((i) => this.http.get(appConfig.apiUrl + '/replies/allreplies'));
  }


getUploads() {
  return this.http.get(appConfig.apiUrl + '/forums/getUploads');
}

  getDownloadFile(id) {
    return this.http.get(appConfig.apiUrl + '/api/download/' + id);
  }


  create(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/forums/create', forum);
  }
  createUpload(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/forums/createUpload', forum);
  }

  createGroup(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/forums/createGroup', forum);
  }

  updateGroup(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/forums/updateGroup', forum);
  }

  getGroups() {
    return this.http.get(appConfig.apiUrl + '/forums/getGroups');
  }

  createreply(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/replies/createreply', forum);
  }

  getSinglereply(forumId) {
    return this.timer
      .flatMap((i) => this.http.get(appConfig.apiUrl + '/replies/getByforumId/' + forumId));
  }

  getSinglereplyStatic(forumId) {

     return this.http.get(appConfig.apiUrl + '/replies/getByforumId/' + forumId);
  }

  updateReply(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/replies/updateReply', forum);
  }
  update(forum: Forum) {
    return this.http.post(appConfig.apiUrl + '/forums/update', forum);
  }
}
