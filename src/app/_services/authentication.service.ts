﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { appConfig } from '../app.config';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient,  private firebaseAuth: AngularFireAuth, private AngularFirestore: AngularFirestore) { }

  login(email: string, password: string) {
    //get userdetails from firebase
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Nice, it worked!');
        return value;
      })
      .catch(err => {
        console.log('Something went wrong:',err.message)
        return err;
      });
      
    };
  //   return this.http.post<any>(appConfig.apiUrl + '/users/authenticate', { username: username, password: password })
  //     .map(user => {
  //       // login successful if there's a jwt token in the response
  //       console.log(user);
  //       if (user && user.token) {

  //         // store user details and jwt token in local storage to keep user logged in between page refreshes
  //         localStorage.setItem('currentUser', JSON.stringify(user));
  //         //Set user status as loggedIn

  //         //Update login database to add currentUser and logged in time

  //       }

  //       return user;
  //     });
  // }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }
}
