import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { appConfig } from '../app.config';
import { Event } from '../_models/index';
@Injectable()
export class EventsService {

  constructor(private http: HttpClient,  private firestore: AngularFirestore) { }


  //Remove all ?
  getAllEvents() {
    //  return this.firestore.collection('events').get();
     return this.firestore.collection('events').snapshotChanges();
  }
  getSingle(_id: string) {
      return this.http.get(appConfig.apiUrl + '/events/single/' + _id);
  }

  getSingleFire(id) {
    return  this.firestore.collection('events').doc(id).valueChanges();
  }
  getSingleFireMod(id) {
    return  this.firestore.collection('events').doc(id).valueChanges();
  }

  update(event: Event) {
    this.firestore.collection('events').doc(event._id).set(event);
    return this.firestore.collection('events').snapshotChanges();
    // return this.http.put(appConfig.apiUrl + '/events/update/' + event._id, event);
  }
 //Remove all?

  // Get count of discussions
  countDiscussions() {
    return this.firestore.collection('forums').get();
  }
}
