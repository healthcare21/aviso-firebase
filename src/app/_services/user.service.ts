﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { appConfig } from '../app.config';
import { User } from '../_models/index';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/mergeMap'


@Injectable()
export class UserService {
  constructor(private http: HttpClient,  private firestore: AngularFirestore, private afAuth: AngularFireAuth) { }
  timer = Observable.timer(0, 5000);

  getAll() {
    return this.http.get<User[]>(appConfig.apiUrl + '/users');
  }

  getAllFire() {
    return  this.firestore.collection('user').snapshotChanges();
  }

  createUserFire(user) {
    this.firestore.collection('user').add(user);
    return user
  }

  getUserFire(emailVar){ //get single user from email
    return  this.firestore.collection('/user', ref => ref.where('email', '==', emailVar)).get();
  }
  getUserFireId(id){ //get single user from email
    return  this.firestore.collection('user').doc(id).valueChanges();
  }
  getUserFireemail(id){ //get single user from email
    return  this.firestore.collection('user').doc(id).valueChanges();
  }

  getUserFireloggin(emailVar){ //get single user from email
    return  this.firestore.collection('/user', ref => ref.where('email', '==', emailVar)).valueChanges({idField: 'customIdName'});
  }

  updateFire(user: User, id) {
    this.firestore.collection('user').doc(id).set(user);
    return true;
  }

  updatePassword(password) {
    console.log('Password', password);
    //this.firestore.collection('user').doc(id).set(user);

    const user = this.afAuth.auth.currentUser;

    const newPassword = 'Shouldbesecure'
  
    user.updatePassword(newPassword).then(function() {
      // Update successful.
    }).catch(function(error) {
      // An error happened.
    });

    
  }


  // getAllPoll() {
  //   return this.timer
  //     .flatMap((i) => this.http.get<User[]>(appConfig.apiUrl + '/users'));

  // }

  createmetricsFire(metricData, DateStamp) {
      //find user by _id and update loggedin status
      this.firestore.collection('user').doc(metricData._id).update(
        {
          loggedin: true
        }
      );
      return true;

  }

  setloggedInFalse(metricData, Datemp) {
    //Shoujld combine the two above?
    this.firestore.collection('user').doc(metricData._id).update(
      {
        loggedin: false
      }
    );
    return true;

}


    
  createmetrics(loginmetrics) {
    return this.http.post(appConfig.apiUrl + '/loginmetrics/create', loginmetrics);
  }

  getMetrics() {
    return this.http.get(appConfig.apiUrl + '/loginmetrics/getAllforumIDS');
  }


  getById(_id: string) {
    return this.http.get(appConfig.apiUrl + '/users/' + _id);
  }
  getSingle(_id: string) {
    return this.http.get(appConfig.apiUrl + '/users/single/' + _id);
  }
  getByemail(email: string) {
    return this.http.get(appConfig.apiUrl + '/users/byemail/' + email);
  }

  updatePasswordwithString(password) {
    return this.http.get(appConfig.apiUrl + '/users/updatePasswordwithString/' + password._id, password);
  }


  create(user: User) {
    return this.http.post(appConfig.apiUrl + '/users/register', user);
  }

  update(user: User) {
    return this.http.put(appConfig.apiUrl + '/users/' + user._id, user);
  }

  delete(_id: string) {
    return this.http.delete(appConfig.apiUrl + '/users/' + _id);
  }

  sendemail(email) {
    return this.http.post(appConfig.apiUrl + '/send-email', email);
  }




}
