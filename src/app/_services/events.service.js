"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var app_config_1 = require("../app.config");
var EventsService = /** @class */ (function () {
    function EventsService(http) {
        this.http = http;
    }
    EventsService.prototype.getAllEvents = function () {
        return this.http.get(app_config_1.appConfig.apiUrl + '/events/allevents');
    };
    EventsService.prototype.getSingle = function (_id) {
        return this.http.get(app_config_1.appConfig.apiUrl + '/events/single/' + _id);
    };
    EventsService.prototype.update = function (event) {
        return this.http.put(app_config_1.appConfig.apiUrl + '/events/update/' + event._id, event);
    };
    EventsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], EventsService);
    return EventsService;
}());
exports.EventsService = EventsService;
//# sourceMappingURL=events.service.js.map