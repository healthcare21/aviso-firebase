﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptorProvider, ErrorInterceptorProvider } from './_helpers/index';
import { AlertService, AuthenticationService, UserService, EventsService, ForumsService, ObservableService } from './_services/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { Globals } from './globals';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { ModrequiresPipe } from './pipes/modrequires.pipe';

 import { UserupdateComponent } from './userupdate/userupdate.component';
 import { UserdetailsComponent } from './userdetails/userdetails.component';
 import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SanitizePipe } from './pipes/sanitize.pipe';
import { FilterbyAuthorPipe } from './pipes/filterby-author.pipe';
import { UserwarningComponent } from './userwarning/userwarning.component';
import { TotalpipePipe } from './pipes/totalpipe.pipe';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { EmailServiceService } from './singleforum/email-service.service';
import { AtNamefunctionsService } from './singleforum/at-namefunctions.service';
import { LikesPipe } from './pipes/likes.pipe';
import { UnlikePipe } from './pipes/unlike.pipe';
import { TotallikesPipe } from './pipes/totallikes.pipe';
import { routing } from './app.routing';
import { PrivacyComponent } from './privacy/privacy.component';
import { FileUploadModule } from 'ng2-file-upload';

import { environment } from '../environments/environment.prod';
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule, AngularFirestore,  } from "@angular/fire/firestore";
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireAuth } from "@angular/fire/auth";

import { CacheInterceptor } from './_services/interceptor';

// import { UploadresComponent } from './uploadres/uploadres.component';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
     NgbModule.forRoot(),
    HttpModule,
    routing,
    FileUploadModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    NavigationComponent,
    FooterComponent,
    ModrequiresPipe,
     UserupdateComponent,
     UserdetailsComponent,
    SanitizePipe,
    FilterbyAuthorPipe,
    UserwarningComponent,
    TotalpipePipe,
    ForgotPasswordComponent,
    LikesPipe,
    UnlikePipe,
    TotallikesPipe,
    PrivacyComponent ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    JwtInterceptorProvider,
    ErrorInterceptorProvider,
    EventsService,
    ForumsService,
    Globals,
    EmailServiceService,
    AtNamefunctionsService,
    ObservableService,
    AngularFireAuth,
    AngularFirestore,
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
