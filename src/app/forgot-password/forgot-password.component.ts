import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { UserService } from '../_services/index';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Globals } from '../globals'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  emailsubscribe: any;
  constructor(private userService: UserService, private router: Router, public globals: Globals, private renderer: Renderer2, private afAuth: AngularFireAuth) { }

  ngOnInit() {
    this.renderer.addClass(document.body, 'shortbackground');
  }

  public submitemail(username) {


    let usernameVar = username.value;
    // return this.afAuth.auth.sendPasswordResetEmail(
    //   usernameVar, 
    //   { url: 'http://localhost:4200/auth' }); 

      this.afAuth.auth.sendPasswordResetEmail(usernameVar, {url : this.globals.curDomain}).then(function() {
        // Email sent.
        alert('A reset password email has been sent to you, please follow the instructions on the email to reset your password');
        this.router.navigate(['/login']);
      }).catch(function(error) {
        // An error happened.
      });
  
    } 

    

    // this.userService.getByemail(username.value).subscribe(singleusers => {
    //   if (singleusers) {
    //     if (singleusers[0].notknown !== 'true') {
    //       var datesend = { _id: singleusers[0].notknown, password: 'random' };
    //       // this.userService.updatePassword(datesend).subscribe(paswordHash => {
    //       //   this.sendemailadmin(username.value, paswordHash);
    //       //   alert('A new password has been sent to you, please reset this in your Account details');
    //       //   this.router.navigate(['/login']);
    //       // })
    //     } else {
    //       alert('Unfortunately, we cannot find that email address, please try again or contact support');
    //     }
    //   } else {
    //     alert('Unfortunately, we cannot find that email address, please try again or contact support');
    //   }
    // });
  //}

  private sendemailadmin(email, password) {
    var bodyPP = {
      "to": email,
      "body": "Hello, <br/>Your new password is, as follows:<br/><br/>" + password + "<br/><br/>You can reset this to something more memorable in your account details once you have logged in.<br/><br/>King regards,<br/><br/>The Aviso Admin Team",
      "subject": this.globals.sitename + " - Password reset"
    };
    this.userService.sendemail(bodyPP).subscribe(event => { console.log(event); });
  }

  ngOnDestroy() {
    if (this.emailsubscribe) {
      this.emailsubscribe.unsubscribe();
    }
  }

}
