import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../globals'
import { AlertService, UserService, AuthenticationService } from '../_services/index';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
const URL = 'http://localhost:4000/api/upload';

@Component({
  selector: 'app-userupdate',
  templateUrl: './userupdate.component.html',
  styleUrls: ['./userupdate.component.scss']
})
export class UserupdateComponent implements OnInit {
  sub: any;
  id: any;
  form: any;
  model: any;
  currentUser: any;
  passwordLength: boolean;
  passwordCharacter: boolean;
  avatarImg: null;
  constructor(public globals: Globals, private route: ActivatedRoute, private fb: FormBuilder, private authenticationService: AuthenticationService, private userService: UserService, private alertService: AlertService, private router: Router, ) {
    this.createForm();
    this.passwordCharacter = false;
  }

  ngOnInit() {
    this.passwordLength = false; this.passwordCharacter = false;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.loadUser(params['id']);
    });

  }
  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      var fileExtension = file.name.split('.').pop();
      if (fileExtension == 'jpg' || fileExtension == 'png' || fileExtension == 'jpeg' || fileExtension == 'gif') {
        if (file.size < 250000) {
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.form.get('avatar').setValue({
              filename: file.name,
              filetype: file.type,
              value: reader.result.split(',')[1]
            });
            this.avatarImg = reader.result.split(',')[1]
            alert('User avatar changed successfully');
          };
        } else {
          alert('Please keep you file size under 250kb');
        }
      } else {
        alert('Please use a file that is either jpg, jpeg, png or gif formats');
      }
    }
  }

  createForm() {
    this.form = this.fb.group({
      _id: '',
      title: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: [''],
      biography: [''],
      username: ['', Validators.required],
      eventid: [''],
      password_r: [''],
      authority: ['doctor'],
      avatarImg: null,
      loggedin: [''],
      optOut: [''],
      token: [''],
    });
  };
  clearFile(){
    //Remove avatar from form
    this.form.avatarImg = null;
    this.avatarImg = null;
  }

  populateForm(data) {
    this.form = this.fb.group({
      _id: data._id,
      title: [data.title],
      firstName: [data.firstName, Validators.required],
      lastName: [data.lastName, Validators.required],
      email: [data.email, Validators.required],
      password: [''],
      biography: [data.biography],
      username: [data.username, Validators.required],
      eventid: [data.eventid],
      optOut: [data.optOut],
      password_r: [''],
      authority: [data.authority],
      avatar: [data.avatar],
      loggedin: [data.loggedin],
      token: [data.token]
    });
    this.avatarImg = data.avatar.value;
  }
  checkPassword(password) {
    //alert('password'+ password.value);
    if (/^[a-zA-Z0-9]*$/.test(password.value) == true) {
      this.passwordCharacter = false;
    } else {
      this.passwordCharacter = true;
    }
    if (password.value.length >= 6) {
      this.passwordLength = true;
    } else {
      this.passwordLength = false;
    }


  }

  private loadUser(curId) {
    this.userService.getUserFireemail(curId).subscribe(singleusers => { 
      this.currentUser = singleusers;
      // this.userId = singleusers[0].payload.doc.id;
     this.model = singleusers; 
     this.populateForm(singleusers); 
    });
  }


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  updateUser(curId, password, email) {
    //  localStorage.setItem('currentUser', JSON.stringify(this.form.value));
    if (this.validateEmail(email.value)) {
      this.userService.updateFire(this.form.value,this.id);
      localStorage.setItem('currentUser', JSON.stringify(this.form.value));
        this.alertService.success('Update successful', true);
        alert('Your account details have been updated');
      
    } else {
      alert('Please enter a valid email address');
    }
  }
}
