import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadresComponent } from './uploadres.component';

describe('UploadresComponent', () => {
  let component: UploadresComponent;
  let fixture: ComponentFixture<UploadresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
