import { Component, OnInit, Input } from '@angular/core';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import {  ForumsService, UserService, AlertService, } from '../_services/index';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
const URL = 'http://localhost:4000/api/upload';
import { appConfig } from '../app.config';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorage } from 'angularfire2/storage';


@Component({
  selector: 'app-uploadres',
  templateUrl: './uploadres.component.html',
  styleUrls: ['./uploadres.component.scss']
})
export class UploadresComponent implements OnInit {
  @Input() inputid: any;
  public uploader: FileUploader = new FileUploader({url: appConfig.apiUrl+'/api/upload', itemAlias: 'photo'});

  dateVar: any;
  users: any;
  public myForm: FormGroup;
  show: boolean;
  groups: any;
  alert: boolean; ref: any; task: any; uploadProgress: any; downloadURL: any;
  percentagecomplete: any; sub: any; id: any;

  constructor(private afStorage: AngularFireStorage, private ForumService: ForumsService, private userService: UserService, private alertService: AlertService, private _fb: FormBuilder,private route: ActivatedRoute ) {
    
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      // if(this.id){
      //   this.idlink = "/addResource/"+this.id;
      // } 
     })

  }

  ngOnInit() {
    this.show = false; this.alert = false; this.percentagecomplete = 0;
    this.createForm();
    let groupsPass = [];
    this.ForumService.getGroupsFire().subscribe(groups => { 
      //remove payload
      groups.map(e => {
        groupsPass.push(e.payload.doc.data())
      })
      this.groups = groupsPass;
    
    });
    // this.ForumService.getAllForums().subscribe(forums => { this.groups = forums; });
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false;};
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         response = JSON.parse(response);
        //  alert(location.pathname);
         this.setInitialForm(response);
         //Save data?
      this.ForumService.createUpload(this.myForm.value).subscribe();
      // this.show = false; 
      this.createForm();
      this.alert = true;
     };
     this.uploader.onProgressItem = (progress: any) => {
      console.log(progress['progress']);
      this.percentagecomplete = progress['progress'];
    };
 }

 upload(event) {
  // console.log(event.target.files[0]);
  //this.afStorage.upload('/upload/'+event.target.files[0].name, event.target.files[0]);  
  const randomId = Math.random().toString(36).substring(2);
  // create a reference to the storage bucket location
  this.ref = this.afStorage.ref('/upload/'+event.target.files[0].name);
  // the put method creates an AngularFireUploadTask
  // and kicks off the upload
  this.task = this.ref.put(event.target.files[0]).then(() => {
  // this.uploadProgress = this.task.percentageChanges();
  const ref = this.afStorage.ref('/upload/'+event.target.files[0].name);
         const downloadURL = ref.getDownloadURL().subscribe(url => { 
         const Url = url; // for ts
         this.downloadURL = url // with this you can use it in the html
      //   this.myForm.controls.downloadUrl = url;
       //  console.log(this.myForm.controls);
       
         //Add this to file uploads
         })
  })
}

addFile(){
	console.log(this.myForm.value);
	if(this.myForm.value.title && this.myForm.value.downloadUrl || this.myForm.value.link){
    this.myForm.value.downloadUrl = (this.myForm.value.downloadUrl ? this.myForm.value.downloadUrl : '')
		this.ForumService.addFile(this.myForm.value);
		this.alertService.success('Added resource successfully', true);
		this.alert = false;
	} else {
		alert('Please fill in all fields and select a file/link to upload');
	}
	//this.router.navigate(['/admin/users']);

}
 createForm() {
   if(this.inputid){ this.id = this.inputid};
  this.dateVar = new Date();
  this.myForm = this._fb.group({
    date: this.dateVar,
    filename: [''],
    title: [''],
    link: [''],
    forumId: this.id,
    downloadUrl: '',
    groupArray: this._fb.array([
      //  this.initAbstracts(),
    ]),
  })
  this.addGroup();

}
continue(){
   if(this.myForm.controls.title.value.length != 0){
    this.show = true;
    this.alert = false;
  } else {
    alert('Please enter a title');
  }


}

addGroup(){

  const control = <FormArray>this.myForm.controls['groupArray'];
  control.push(
    this._fb.group({
      groupItem:['']
    })
  );
}
setInitialForm(data) {
  
  this.dateVar = new Date();
  this.myForm = this._fb.group({
    date: this.dateVar,
    title: this.myForm.controls.title.value,
    link: '',
    filename: data,
    forumId: this.id,
    downloadUrl : this.downloadURL,
    groupArray: this._fb.array([
      this.myForm.controls.groupArray
    ]),
  })

}


}
