import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ContactComponent } from '../contact/contact.component';

import { routing } from './modules.routing';

@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [ ContactComponent]
})
export class ModulesModule { }
