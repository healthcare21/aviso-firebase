import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WebexComponent } from '../webex/';
import { PrivacyComponent } from '../privacy/privacy.component';
import { ContactComponent } from '../contact/contact.component';

const routes: Routes = [
  { path: 'webex', component: WebexComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'contact', component: ContactComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
