import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../../home/index';
import { ForumComponent } from '../../forum/forum.component';
import { SingleforumComponent } from '../../singleforum/singleforum.component';
 import { UserdetailsComponent } from '../../userdetails/userdetails.component';
 import { UserupdateComponent } from '../../userupdate/userupdate.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'forum', component: ForumComponent },
  { path: 'forum/:id', component: SingleforumComponent },
  //  { path: 'userdetails/:id', component: UserdetailsComponent },
   { path: 'user-edit/:id', component: UserupdateComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
