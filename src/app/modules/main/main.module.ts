import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { SidebarModule } from '../../sidebar/sidebar.module';
import { ReplysanitizeModule } from '../../replysanitize/replysantize.module';
import { HomeComponent } from '../../home/index';
import { ForumComponent } from '../../forum/forum.component';
import { SingleforumComponent } from '../../singleforum/singleforum.component';
import { OrderPipePipe } from '../../pipes/order-pipe.pipe';
import { DateFormatPipe } from '../../pipes/date-format.pipe';
import { ModerationPipe } from '../../pipes/moderation.pipe';
import { EditorModule } from '@tinymce/tinymce-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterByuserPipe } from '../../pipes/filter-byuser.pipe';
import { PaginationPipe } from '../../pipes/pagination.pipe';
import { ByForumIdPipe } from '../../pipes/by-forum-id.pipe';
import { ResourcesFilter } from '../../pipes/filter_resources';
import { IdleComponent } from '../../idle/idle.component';
import { CommentComponent } from '../../singleforum/comment/comment.component'
import { WebexComponent } from '../../webex/';
import { ModalComponent } from '../../singleforum/modals/modaladd';

// import { LimittextPipe } from '../../pipes/limittext.pipe';

import { routing } from './main.routing';
@NgModule({
  imports: [
    CommonModule, routing, SidebarModule, EditorModule, FormsModule, ReplysanitizeModule
  ],
  declarations: [ ModalComponent, CommentComponent, WebexComponent, IdleComponent, ByForumIdPipe, HomeComponent, ForumComponent, SingleforumComponent, OrderPipePipe, DateFormatPipe, ModerationPipe, FilterByuserPipe,ResourcesFilter, PaginationPipe],

})
export class MainModule { }
