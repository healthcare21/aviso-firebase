"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var index_1 = require("./home/index");
var index_2 = require("./login/index");
var index_3 = require("./register/index");
var index_4 = require("./_guards/index");
var users_component_1 = require("./admin/users/users.component");
var user_edit_component_1 = require("./admin/user-edit/user-edit.component");
var event_edit_component_1 = require("./admin/event-edit/event-edit.component");
var appRoutes = [
    { path: '', component: index_1.HomeComponent, canActivate: [index_4.AuthGuard] },
    { path: 'login', component: index_2.LoginComponent },
    { path: 'register', component: index_3.RegisterComponent },
    { path: 'admin/users', component: users_component_1.UsersComponent, canActivate: [index_4.AuthGuard] },
    { path: 'admin/users-edit/:id', component: user_edit_component_1.UserEditComponent, canActivate: [index_4.AuthGuard] },
    { path: 'admin/event-edit/:id', component: event_edit_component_1.EventEditComponent, canActivate: [index_4.AuthGuard] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map
