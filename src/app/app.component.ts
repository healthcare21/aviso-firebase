﻿import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

 
 declare var gtag;

@Component({
  moduleId: module.id,
  selector: 'app',
  templateUrl: 'app.component.html'
})

export class AppComponent {


  constructor(router: Router) {
    const navEndEvents = router.events.pipe(
      filter(
        event => event instanceof NavigationEnd
      )
    );

    navEndEvents.subscribe(
      (event: NavigationEnd) => {
        gtag('config', 'UA-151250276-1', {
          page_path: event.urlAfterRedirects,
        });
      }
    );
  }



  ngOnInit() {
 
  
    //Check for auth????



    // this.router.events.distinctUntilChanged((previous: any, current: any) => {
    //   // Subscribe to any `NavigationEnd` events where the url has changed
    //   if (current instanceof NavigationEnd) {
    //     return previous.url === current.url;
    //   }
    //   return true;
    // }).subscribe((x: any) => {
    //   gtag('config', 'UA-119136819-1', { 'page_path': x.url });
    // });
  }

}
