import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../_models/index';
import { UserService, EventsService, ForumsService } from '../_services/index';
import { Globals } from '../globals'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  currentUser: User;
  eventDetails: any;
  forums: any;
  forumServ: any;

  constructor(public globals: Globals, private EventsService: EventsService, private ForumService: ForumsService, private router: Router, ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //   router.events.subscribe((val) => {
    //     if (val.url == '/login'){
    //       this.currentUser ='';
    //     }
    //
    // });
  }

  ngOnInit() {
    this.getEvent();

    if (this.router.url == '/login') {
      this.currentUser = null;
    }
  }

  private getEvent() {
    if (this.currentUser != null) {
      this.EventsService.getSingleFire(this.globals.event).subscribe(event => { this.eventDetails = event });
    }
  }

  private getForums() {
  }

  ngOnDestroy() {
    //this.forumServ.unsubscribe();
  }


}
