﻿import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../globals'
import { AlertService, UserService } from '../_services/index';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EmailServiceService } from '../singleforum/email-service.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { group } from '@angular/core/src/animation/dsl';

const URL = 'http://localhost:4000/api/upload';
@Component({
  moduleId: module.id,
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss']
})

export class RegisterComponent {
  model: any = {};
  loading = false;
  eventid: any = [];
  sub: any;
  form: FormGroup;
  passwordLength: boolean;
  passwordCharacter: boolean;
  formvalid: boolean;
  email: any;
  avatarImg : null



  constructor(
    private router: Router,
    private userService: UserService,
    private route: ActivatedRoute,
    private globals: Globals,
    private fb: FormBuilder,
    private firebaseAuth: AngularFireAuth,
    private EmailServiceService: EmailServiceService,
    private _http: HttpClient,
    private alertService: AlertService) {
    this.createForm();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.eventid = [this.globals.event];
      this.model = ({ authority: 'doctor', eventid: this.eventid });
      this.formvalid = true;
    });
  };

  clearFile(){
    //Remove avatar from form
    this.avatarImg = null;
  }

  checkPassword(password) {
    //alert('password'+ password.value);
    if (/^[a-zA-Z0-9]*$/.test(password.value) == true) {
      this.passwordCharacter = false;
    } else {
      this.passwordCharacter = true;
    }
    if (password.value.length >= 6) {
      this.passwordLength = true;
    } else {
      this.passwordLength = false;
    }
  }

  


  private sendemail(email, username, password) {



//  this._http.post('http://aviso.healthcare21.website/php_scripts/index.php' , email, {}).map((response: Response) =>{
//     console.log ('response',response.json());
//     })

let formdata = new FormData();

let message =  "You have successfully registered to the Discussion and resource centre - your login details are: \r\n\r\nusername - " + email + "\r\n\r\npassword - " + password + "\r\n\r\nPlease login here- " + this.globals.curDomain;


formdata.append('email', email);
formdata.append('message', message);


this._http.post('http://aviso.healthcare21.website/php_scripts/index.php', formdata).subscribe(
  res => {
    console.log('res',res);
  },
  err => {
    console.log("Error occured", err);
  }
);


    // this._http.post('http://aviso.healthcare21.website/php_scripts/index.php', {
    //   "from": "foo",
    //   "email": email
    // })
    //   .subscribe(
    //     res => {
    //       console.log('res',res);
    //     },
    //     err => {
    //       console.log("Error occured");
    //     }
    //   );


    // ...
    
    // const headers = new HttpHeaders({
    //   'enctype': 'multipart/form-data',
    //   'Authorization': `Basic ${btoa('api:key-d94f370b0586d4480c9a8fc247e81250')}`
    // });
    
    // const formData = new FormData();
    // formData.append('from', 'Mailgun Sandbox <masterclass-meeting.com>')
    // formData.append('to', 'webape2gmail.com');
    // formData.append('subject', 'Hello');
    // formData.append('text', 'This is cool !');
    // formData.append('email', email);
    
    // this._http
    // .post(
    //   'http://aviso.healthcare21.website/php_scripts/index.php',
    //   formData, 
    //   { headers }
    // ).subscribe(
    //   res => { console.log('res : ', res); },
    //   err => { console.log('err : ', err); }
    // );

    // this._http.post('https://api.mailgun.net/v3/MY_MAILGUN_DOMAIN.mailgun.org/messages', {
    //   from: 'foo',
    //   to: email
    // })
    //   .subscribe(
    //     res => {
    //       console.log(res);
    //     },
    //     err => {
    //       console.log("Error occured");
    //     }
    //   );


  //   var headers = new Headers(); 
  //   headers.append("Authorization": "Basic "+btoa("api:key-API_KEY");
  //       var url="https://api.mailgun.net/v3/MY_MAILGUN_DOMAIN.mailgun.org/messages";
  //   var mail = {
  //     from : "text",
  //     to : "text",
  //     subject : "text",
  //     text : "text"
  //   };
  //  this._http.post(url, message, {headers: headers});
    // var bodyPP = {
    //   "to": email,
    //   "body": "You have successfully registered to the Discussion and resource centre - your login details are: \r\n\r\nusername - " + username + "\r\n\r\npassword - " + password + "\r\n\r\nPlease login  <a href='" + this.globals.curDomain + "'>here</a>",
    //   "subject": "Registration to the Discussion and resource centre"
    // };
    // this.email = this.userService.sendemail(bodyPP).subscribe(event => { console.log(event); this.email.unsubscribe(); });
  }


  createForm() {
    this.eventid = [this.globals.event];
    this.form = this.fb.group({
      title: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      password_r: [''],
      username: [''],
      biography: [''],
      eventid: this.eventid,
      authority: ['doctor'],
      register: ['', Validators.required],
      avatar: null
    });
  }

  onSubmit() {
    const formModel = this.form.value;
    // this.loading = true;
    // In a real-world app you'd have a http request / service call here like
    // this.http.post('apiUrl', formModel)
    setTimeout(() => {
      console.log(formModel);
      alert('done!');
      // this.loading = false;
    }, 1000);
  }


  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      if (file.size < 250000) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.form.get('avatar').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          });
          this.avatarImg = reader.result.split(',')[1]

        };
      } else {
        alert('Please keep you file size under 250kb');
      }
    }
  }

  register() {
    if (this.form.valid == true) {
      var password = this.form.value.password;
      var password_r = this.form.value.password_r;
      if (password != password_r || !this.passwordCharacter || !this.passwordLength) {
        alert('Please make sure the passwords match and that the password meets the required security messures');
      } else {
        this.loading = true;
        this.firebaseAuth
        .auth
        .createUserWithEmailAndPassword(  this.form.value.email, this.form.value.password)
        .then(value => {
        
        // let groupAll = [];
        // groupAll.push( {group: [{'title' : "All"}]} );
        this.form.value.group = [{'title' : "All"}];

        this.userService.createUserFire(this.form.value)
        
        //this.EmailServiceService.sendemailadminRegister(this.globals.adminEmail, this.globals.adminFirstname, this.globals.adminSurname, this.form.value.firstName, this.form.value.lastName);
        this.sendemail(this.form.value.email, this.form.value.username, this.form.value.password)
         this.alertService.success('Registration successful', true);
        this.router.navigate(['/login']);
           
            
        })
        .catch(err => {
          alert('An error occured - '+err.message);
          this.alertService.error(err.message);
    
        });
      } 
    } else {
      //Form is not valid
      this.formvalid = false;
      alert('Please fill in all required fields.')
    }


  }
  ngOnDestroy() {

  }
}
