import { Component, OnInit, HostListener } from '@angular/core';
import { EventsService } from '../_services/index';
import { Globals } from '../globals';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import * as moment from 'moment';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  sitename: any;
  slogan: any;
  currentUser: any;
  eventservice: any;
  hoursDif: any;
  daysDif: any;
  minutesDif: any;
  displayDate: any;
  eventDetails: any;
  toggleNav:any;
  constructor(public globals: Globals, private EventsService: EventsService, private router: Router, private firebaseAuth: AngularFireAuth) {
    //Check that the current route is not /login - if it is clear the user data.
    this.toggleNav = 'closed'
    router.events.subscribe((event: Event) => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      // console.log('currentUser', this.currentUser );
      if (event instanceof NavigationEnd) {

        if (event.url == '/login') {
          this.currentUser = null;
        } else {
          this.getEvent();
        }
      }
    });
  }

  navbarCollapsed = true;

  toggleNavbarCollapsing() {
    this.navbarCollapsed = !this.navbarCollapsed;
  }
  toggleState() {
    if( this.toggleNav === 'closed'){
      this.toggleNav = 'open';
    } else {
      this.toggleNav = 'closed';
    }
  }
  closeNav = () => {this.toggleNav = 'closed'}

  ngOnInit() {
    this.sitename = [this.globals.sitename];
    this.slogan = [this.globals.siteslogan];
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.getEvent();

    this.isLoggedIn().subscribe(
      (user)=>{
        if(!user){
          this.router.navigate(['/login']);
        }
     })
  }

  isLoggedIn() {
    return  this.firebaseAuth.authState;
 }

  private getEvent() {
    if (this.currentUser) {
      this.eventservice = this.EventsService.getSingleFire(this.globals.event).subscribe(event => {
        this.eventDetails = event;
        var today = moment(new Date());
        var end = moment(this.eventDetails.enddate, "MM/DD/YYYY");
        var duration = moment.duration(end.diff(today));
        this.displayDate = end.format("DD MMM YYYY");
        this.daysDif = Math.ceil(duration.asDays());
        this.hoursDif = Math.ceil(duration.asHours());
        this.minutesDif = Math.ceil(duration.asMinutes());
        this.minutesDif = Number(this.minutesDif).toLocaleString()
      });
    }
  }

}
