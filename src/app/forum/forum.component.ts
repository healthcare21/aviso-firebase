import { Component, OnInit } from '@angular/core';
import { UserService, EventsService, ForumsService, AlertService } from '../_services/index';
import { Globals } from '../globals'
import { FormBuilder, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import * as _ from "lodash";

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})
export class ForumComponent implements OnInit {
  forums: any;
  webexurl: any;
  eventDetails: any;
  myForm: any;
  currentUser: any;
  users: any;
  totalUsers: any;
  sortorder: any;
  forumServ: any;
  userServ: any;
  forumReply: any;
  allReplies: any;
  showIdle: any; singleuser: any;
  constructor(private userService: UserService, private ForumService: ForumsService, private _fb: FormBuilder, public globals: Globals, private EventsService: EventsService, private _sanitizer: DomSanitizer, private AlertService: AlertService, public router: Router) {
    this.createForm();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (!this.currentUser) {
     this.router.navigate(['/login']);
    }
  }



  ngOnInit() {
     this.loadUser(this.currentUser);
    // this.getEvent();
    // this.loadAllUsers();
  
    var sortorderVar = localStorage.getItem('sortorder');
    if (sortorderVar) {
      this.sortorder = sortorderVar;
    } else {
      this.sortorder = 'default';
    }
    //this.sendemail();
  }

  private loadUser(curId) {
    this.userService.getUserFire(curId.email).subscribe(singleusers => { 
       this.singleuser = singleusers.docs[0].data();
       this.getForums();
    });


  }

  private toggleforumorder() {
    if (this.sortorder == 'default') {
      localStorage.setItem('sortorder', 'earliest');
      this.sortorder = "earliest";
    } else {
      this.sortorder = 'default';
      localStorage.setItem('sortorder', 'default');
    }
    this.forumServ.unsubscribe();
    this.getForums();
  }

  private getForums() {
    //get forums and then filter by users groups
    let forumPass =[]; 
    this.forumServ = this.ForumService.getallForumsfire().subscribe (forumre => { 
      forumre.map((e:any, index) => {
       
        let forumData = e.payload.doc.data();
        if(!forumData._id){
          // forumpass._id = e.payload._id;
          forumData._id = forumre[index].payload.doc.id;
          // forumpass._id = forumre[0].payload.doc.id;
        }

        forumPass.push(forumData)
   
        this.ForumService.getRepliesByID(forumPass[index]._id).subscribe (replies => { 
   
            if (this.globals.moderation === 'on'){
              let x = 0;
  
              replies.forEach(forumReply => {
                let dataReply = forumReply.payload.doc.data();
                if(dataReply['inmoderation'] == 'moderated'){
                    x++;
                }
    
              });
              forumPass[index].repliesNum = x;
             
            } else {
              forumPass[index].repliesNum = replies.length;
            }

         });
      })
    });
    this.forumServ = this.ForumService.getallForumsfire().subscribe(forums => { this.filterByGroups(forumPass);  });
    this.forumReply = this.ForumService.getAllrepliesFire().subscribe(replies => {
      // console.log('Replies', replies)
       this.allReplies = replies;
    })



  }
  trackByFn(index, item) {
    return index; // or item.id
  }

  private filterByGroups(forums){
  //  this.singleuser = this.singleuser.docs[0].data()
    //Get users allowed groups
    let finalArr = [];
    //Loop around forums and return only allowed forum groups
    for (var k = 0; k < forums.length; k++) {
      let forumarry1 = forums[k].groups;
      //Check for an id??


      if(!forums[k]._id) {
        // alert(forums[k].name);
        // console.log('forums[k]',forums)
      }


      let lock = false;
      if(forumarry1){
          if(forumarry1.length == 0){ finalArr.push(forums[k]); lock = true; } //Display forum if it has no groups assigned
          if(this.singleuser.title === 'All'){
            if(!lock) {
              finalArr.push(forums[k]); lock = true 
             };
          }
          if (!forums[k].groups){
            finalArr.push(forums[k]); //if no groups are set return the forum??   
            lock = true;
          } else {
              //Check to see if the user has the correct rights
            if(!this.singleuser.group){
              if(!lock){ //No groups set presume all
                finalArr.push(forums[k]);
                lock = true;
              }
            } else {
              let array1 = this.singleuser.group;
              let array2 = forums[k].groups;
              //Loop around users groups..
              for (var l = 0; l < array1.length; l++) {
                var dif = _.findIndex(array2, (o) => { return _.isMatch(o, array1[l]) });
  
                if(dif != -1){
                  if(!lock){
                          finalArr.push(forums[k]);
                          lock = true;
                        }
                }
              }
              
            }
          }
      }
    }
    this.forums = finalArr;
  }

  // private getEvent() {
  //   this.EventsService.getSingle(this.globals.event).subscribe(event => { this.eventDetails = event; this.webexurl = this._sanitizer.bypassSecurityTrustResourceUrl(this.eventDetails.webex) });
  // }



  createForm() {
    this.myForm = this._fb.group({
      forumposts: this._fb.array([

      ])

    })
  }

  setInitialForm(data) {
    for (var i = 0; i < data.length; i++) {
      const control = <FormArray>this.myForm.controls['forumposts'];
      control.push(
        this._fb.group({
          _id: data[i]._id,
          name: data[i].name,
          question: data[i].question,
          username: data[i].username,
          replies: this._fb.array([
          ])
        })
      );
    }
    this.pushReplies(data);
  }

  pushReplies(data) {
    //console.log(data);
    var x = 0;
    for (let formItem of this.myForm.controls['forumposts'].controls) {
      const controlReply = formItem.controls['replies'];
      if (data[x]) {
        for (var i = 0; i < data[x].replies.length; i++) {
          controlReply.push(
            this._fb.group({
              name: data[x].replies[i].name,
              reply: data[x].replies[i].reply
            })
          )
        }
      }
      x++;
    }
  }

  addreply(curIndex) {
    var x = 0;
    for (let formItem of this.myForm.controls['forumposts'].controls) {
      if (x == curIndex) {
        const controlReply = formItem.controls['replies'];
        controlReply.push(
          this._fb.group({
            name: '',
            reply: ''
          })
        );
      }
      x++;
    }
  }

  updateForm(index, name, reply) {
    var x = 0;
    for (let formItem of this.myForm.controls['forumposts'].controls) {
      if (x == index) {
        const controlReply = formItem.controls['replies'];
        controlReply.push(
          this._fb.group({
            name: name.value,
            reply: reply.value
          })
        );
        var returnItem = JSON.stringify(formItem.value);
        this.ForumService.update(formItem.value).subscribe(singleusers => {
          this.AlertService.success('Update successful', true);
          this.router.navigate(['/forum']);
        })
      }
      x++;
    }
  }

  ngOnDestroy() {
    //this.forumServ.unsubscribe();
    //this.userServ.unsubscribe();
  }


}
