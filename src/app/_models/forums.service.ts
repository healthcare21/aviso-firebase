export class Forum {
  reply: string;
  username: string;
  authorref: string;
  date: string;
  forumId: string;
  replytoreply: boolean;
  _id: string;
  forumlabel: string;
}
