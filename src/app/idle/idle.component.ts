import { Component, OnInit } from '@angular/core';
import { Idle } from 'idlejs/dist';
import { Router } from '@angular/router';



@Component({
  selector: 'app-idle',
  templateUrl: './idle.component.html',
  styleUrls: ['./idle.component.scss']
})
export class IdleComponent implements OnInit {
  showIdle: any
  constructor(public router: Router) {

  }



  ngOnInit() {
    this.showIdle = 'false';
    // with predefined events on `document`
    const idle1 =  new Idle()
      .whenNotInteractive()
       .within(300, 1000)
      .do(() => this.triggerbox('idle'))
      .start();

    const idle2 = new Idle()
      .whenNotInteractive()
      .within(600, 1000)
      .do(() => this.logout())
      .start();

  }


  triggerbox(state) {
    if (state == 'idle') {
      this.showIdle = 'truth';
    } else {
      this.showIdle = 'false';
    }
  }

  logout() {
    var index = this.router.url.indexOf("forum");
    //alert("indexOf found String :" + index);
    if (this.showIdle == 'truth') {
      if (index != -1) {
        this.router.navigate(['/login']);
      }
    } 
  }

}
