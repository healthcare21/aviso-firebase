﻿import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AuthGuard } from './_guards/index';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PrivacyComponent } from './privacy/privacy.component';
import {HomeComponent} from './home/home.component'

const appRoutes: Routes = [
  { path: '', loadChildren: 'app/modules/main/main.module#MainModule', canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'home/:id', component: HomeComponent },
  // { path: 'addResource/:id', component: UploadresComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'na/privacy', component: PrivacyComponent },
  { path: 'na', loadChildren: 'app/modules/modules.module#ModulesModule', canActivate: [AuthGuard] },
  { path: 'forgot', component: ForgotPasswordComponent },
  { path: 'admin', loadChildren: 'app/admin/edit-forum/edit-forum.module#EditForumModule', canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' },
 
];

export const routing = RouterModule.forRoot(appRoutes);
