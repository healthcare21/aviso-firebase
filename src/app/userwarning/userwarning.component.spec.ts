import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserwarningComponent } from './userwarning.component';

describe('UserwarningComponent', () => {
  let component: UserwarningComponent;
  let fixture: ComponentFixture<UserwarningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserwarningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserwarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
