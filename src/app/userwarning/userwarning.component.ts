import { Component, OnInit } from '@angular/core';
import { ForumsService } from '../_services/index';
import * as moment from 'moment';

@Component({
  selector: 'app-userwarning',
  templateUrl: './userwarning.component.html',
  styleUrls: ['./userwarning.component.scss']
})
export class UserwarningComponent implements OnInit {

  constructor(private ForumService: ForumsService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
  forumServ: any;
  messages: any;
  forums: any;
  currentUser: any;

  ngOnInit() {
    //Get all forum posts and identify if any have the author included on the forum post
    this.messages = []; this.forums = [];
    this.getForums();
  }
  private getForums() {
    this.forumServ = this.ForumService.getAllForumsstatic().subscribe(forums => { this.forums = forums; this.getUsersMessages(forums); });
  }

  private getUsersMessages(data) {
    this.messages = [];
    var userName = this.currentUser.firstName + ' ' + this.currentUser.lastName;
    for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < data[i].replies.length; j++) {
        for (var k = 0; k < data[i].replies[j].replies.length; k++) {
          if (data[i].replies[j].replies[k].authorref == userName) {
            this.messages.push(data[i]);
            var newDate = moment(data[i].replies[j].replies[k].date).format('MM/DD/YYYY | h:mm:ss');
            this.messages[this.messages.length - 1].dater = newDate;
            //this.forums.push(currentForum);
          }
        }
      }
    }
  }

}
