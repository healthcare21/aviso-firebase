import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userlookup'
})
export class UserlookupPipe implements PipeTransform {

  transform(value: any, args: any, arg2: any): any {
    console.log(args);
    var returnArr = ''; //Arr but it be a string!
    if (arg2) {

      for (let i = 0; i < arg2.length; ++i) {

        if (arg2[i]._id == args) {
          returnArr = arg2[i].firstName + ' ' + arg2[i].lastName;
        }
      }
    }
    console.log(returnArr);
    return returnArr;
  }

}
