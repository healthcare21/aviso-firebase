import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unlike'
})
export class UnlikePipe implements PipeTransform {

  transform(value: any, args1, args2): any {
    var boolean = false;
    if (value) {
      for (var i = 0; i < value.length; i++) {
        if (value[i].replyId == args1 && value[i].userid == args2) {
          boolean = true;

        }

      }
      return boolean;
    } else {
      return boolean;
    }

  }
}
