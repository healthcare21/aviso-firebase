import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'totallikes'
})
export class TotallikesPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var total = 0;
    if (value) {
      for (var i = 0; i < value.length; i++) {
        if (value[i].replyId == args) {
          total++;
        }
      }

      return total;

    }
  }
}
