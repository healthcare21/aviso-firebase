import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormatadmin'
})
export class DateFormatPipeAdmin implements PipeTransform {

  transform(value: any, args?: any): any {

    //var newValue = moment(value).format('MM/DD/YYYY | h:mm:ss');
    var newValue = moment.utc(value).format('MM/DD/YYYY | h:mm:ss');
    //newValue = moment(newValue).format('MM/DD/YYYY | h:mm:ss');
    if (newValue != 'Invalid date') {
      //  console.log(newValue);
      return newValue;
    } else {
      return 'No date set';
    }


  }

}
