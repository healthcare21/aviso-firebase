import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";
import * as moment from 'moment';

@Pipe({
  name: 'orderPipe'
})
export class OrderPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var allReplies =[];
  
      if(value){
        for (var i = 0; i < value.length; i++) {
            if(value[i].date){
              allReplies.push(value[i]);
            }
        }
        allReplies = _.sortBy(allReplies, function(o) {
        
          return moment(o.date.seconds);
         }).reverse();
    }


      if (args != 'default') {
        return allReplies.reverse();
      } else {
        return allReplies;
      }
    }

  

}
