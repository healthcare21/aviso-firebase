import { Pipe, PipeTransform } from '@angular/core';
import {Globals} from '../globals'

@Pipe({
  name: 'moderation'
})
export class ModerationPipe implements PipeTransform {

constructor(private globals: Globals) { }

  transform(value: any, args?: any): any {

    if(this.globals.moderation == 'on') {
            var returnArray = [];
            if(value){

              //console.log(value.length);
              for (var i = 0; i < value.length; i++) {
                if(value[i].inmoderation){
                  if(value[i].inmoderation == 'false'){
                    //console.log('add to array');
                    returnArray.push(value[i]);
                  }
                }
                //console.log(value[i]);
              }
            }

            return returnArray;
    } else {
      return value;
    }
  }

}
