import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByuser'
})
export class FilterByuserPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var returnedArr = [];
    if(args){
    for (var i = 0; i < value.length; i++) {
      var fullName = value[i].firstName + ' ' + value[i].lastName;
     
      var n = fullName.includes(args);
      
      if (n) {
        returnedArr.push(value[i]);
      }
    }
    return returnedArr;
  } else {
    return value;
  }
   
  }

}
