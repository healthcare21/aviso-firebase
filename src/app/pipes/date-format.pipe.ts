import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    //var newValue = moment(value).format('MM/DD/YYYY | h:mm:ss');
      if(value){
        var newValue = moment.unix(value.seconds).format('DD/MM/YYYY | hh:mm:ss');
        //newValue = moment(newValue).format('MM/DD/YYYY | h:mm:ss');
        if (newValue != 'Invalid date') {
     
            var res = newValue.split(" | ");
            let returnRes = '<span>' +res[0]+'</span>'+' | <span>' +res[1]+'</span>';

          return returnRes;
        } else {
          return 'No date set';
        }
      }
    }

}
