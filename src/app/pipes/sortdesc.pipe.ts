import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";

@Pipe({
  name: 'sortdesc'
})
export class SortdescPipe implements PipeTransform {

  transform(value: any, args?: any): any {
  

    return _.reverse(value);
  }

}
