import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  transform(value: any, args?: any, arg2?:any): any {
    //console.log(args);
    if(args){
      let startPage = 0; let endpage = 4;
        if (arg2 != 1){
          startPage = (arg2-1)*5;
          endpage = ((arg2)*5)-1;
        
        }

        let returnArr =[];
        for (var i = 0; i < value.length; i++) {
            if (i >= startPage && i<= endpage){
              returnArr.push(value[i]);
            }
        }


    return returnArr;
  } else {
    return value;
  }
  }

}
