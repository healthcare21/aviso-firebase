import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterforum'
})
export class FilterforumPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      if (args) {
        var returnVar = [];
        for (var i = 0; i < value.length; i++) {
          if (value[i].forumId == args) {
            returnVar.push(value[i]);
          }
        }

        return returnVar;
      }
      return null;
    }
  }

}
