import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'likes'
})
export class LikesPipe implements PipeTransform {

  transform(value: any, args1, args2): any {
    var boolean = true;
    if (value) {
      for (var i = 0; i < value.length; i++) {
        if (value[i].replyId == args1 && value[i].userid == args2) {
          boolean = false;

        }

      }
      return boolean;
    } else {
      return boolean;
    }

  }
}
