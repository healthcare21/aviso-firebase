import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import * as _ from "lodash";

@Pipe({
  name: 'dateSort'
})
export class DateSortPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var allReplies =[];
    if(value){
        for (var i = 0; i < value.length; i++) {
            if(value[i].date){
              allReplies.push(value[i]);
            }
        }
        allReplies = _.sortBy(allReplies, function(o) {
          return moment(o.date.seconds);
         }).reverse();
    }



    return allReplies;
  }

}
