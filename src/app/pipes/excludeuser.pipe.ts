import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";

@Pipe({
  name: 'excludeuser'
})
export class ExcludeuserPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    var b = value.filter(function(item) {
      return item.userid === args;
    });

    return b;//Buzz Buzz
  }

}
