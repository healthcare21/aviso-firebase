import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterbyAuthor'
})
export class FilterbyAuthorPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    //onsole.log(args.length);
    if (args === 'undefined') {
      return value;
    }

    if (args != 'none' || args == null || args === 'undefined') {
      var returnedArr = [];
      for (var i = 0; i < value.length; i++) {
        if (args == value[i].username) {

          returnedArr.push(value[i]); //Definitly push the whole of the first reply to the DOM as user is the author of a primary reply

          //Limit replies of replies by removing replies that are not by the currentAuthor  - after initial loop?
        } else {
          //Test for replies to reply in other Inital replies
          var includePrimary = false;
          if (value[i].replies) {
            for (var j = 0; j < value[i].replies.length; j++) {
              if (!includePrimary) {
                if (args == value[i].replies[j].username) {
                  returnedArr.push(value[i]); //Push to array as primary reply has a reply by this user
                  includePrimary = true;
                }
              }
            }
          }
        }
      }
      //Loop around returnedArr and remove replies of replies by username
      var includeSecondary = [];

      // for (var k = 0; k < returnedArr.length; k++) {
      //   console.log(returnedArr[k]);
      //   var buildarr = [];
      //   for (var l = 0; l < returnedArr[k].length; l++) {
      //     if (args == returnedArr[k].username) {
      //       buildarr.push(returnedArr[k]);
      //     }
      //   }
      //   returnedArr[k] = buildarr;
      // }
      return returnedArr;
    } else {
      return value;
    }

  }

}
