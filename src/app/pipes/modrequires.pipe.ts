import { Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Pipe({
  name: 'modrequires'
})
export class ModrequiresPipe implements PipeTransform {


    transform(value: any, args?: any): any {

      //console.log(value);
              var returnArray = [];

              if(value){
              //  console.log(value.length);
                for (var i = 0; i < value.length; i++) {
                  var tempArray = [];

                  tempArray = value[i];
                  value[i]['id'] = i;
                  if(value[i]['inmoderation']){
                    if(value[i]['inmoderation'] != 'true'){
                      //console.log('add to array');

                      returnArray.push(value[i]);
                    }
                  }
                  //console.log(value[i]);
                }
              }
               //console.log(returnArray);
              return returnArray;

    }

  }
