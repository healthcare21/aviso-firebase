import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limittext'
})
export class LimittextPipe implements PipeTransform {

  transform(value: any, args?: number): any {

    value = value.replace(/<\/?span[^>]*>/g, "");

    if (!args) { var lengthVar = 150; } else { var lengthVar = args; }

    if (value.length > lengthVar) {
      value = value.replace(/<(.|\n)*?>/g, '');
      value = value.slice(0, lengthVar);
      value = value + '....';
      return value;
    } else {
      value = value.replace(/<(.|\n)*?>/g, '');
      return value;
    }

  }

}
