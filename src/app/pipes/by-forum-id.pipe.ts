import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'byForumId'
})
export class ByForumIdPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    var allReplies = [];
    if (value) {
      for (var i = 0; i < value.length; i++) {
        if (value[i].forumId == args) {


          allReplies.push(value[i]);
        }
      }
    }

    return allReplies;
  }

}
