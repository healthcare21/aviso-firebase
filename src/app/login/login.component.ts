﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../globals';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertService, AuthenticationService, UserService } from '../_services/index';
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  moduleId: module.id,
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  role: any;
  currentUser: any;
  dateVar: any;

  constructor(
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    public globals: Globals,
    private userService: UserService,
    private firebaseAuth: AngularFireAuth,
    private AngularFirestore: AngularFirestore,
    private alertService: AlertService) { }

  ngOnInit() {
    this.renderer.addClass(document.body, 'shortbackground');

    // reset login status
    this.dateVar = new Date();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      this.userService.setloggedInFalse(this.currentUser, '');
      
      //Set logged in as false for current user 



      // this.currentUser['loggedin'] = false;
      // //   //Update User set logged as false
      // this.userService.update(this.currentUser).subscribe(singleusers => {
      // });
      // //Set metrics to show logout time
      // var loggedinCreate = { "firstName": this.currentUser.firstName, "lastName": this.currentUser.lastName, "userid": this.currentUser._id, "Date": this.dateVar, "Status": "LoggedOut" };
      // this.userService.createmetrics(loggedinCreate).subscribe(data => { console.log(data); });
      // //End Metrics


    }
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  createmetrics(userDetails){
    const DateStamp = '';
    this.userService.createmetricsFire(userDetails, DateStamp);
  }

    login() {
      this.loading = true;
      //get userdetails from firebase
  
    
      this.firebaseAuth
        .auth
        .signInWithEmailAndPassword(this.model.username, this.model.password)
        .then(value => {
          //Get all from users by email address
          let returnId;
          this.userService.getUserFire(this.model.username) .subscribe(
                  data => {
                     returnId = data.docs[0].data()
                      returnId['_id'] = data.docs[0].id;
                        localStorage.setItem('currentUser', JSON.stringify(returnId));
                        this.router.navigate(['/home', this.model.username]);
                        this.createmetrics(returnId);
                  })
          
          // localStorage.setItem('currentUser', JSON.stringify(userData));
          // this.router.navigate([this.returnUrl]);12

                  
        })
        .catch(err => {
          console.log('Something went wrong:',err.message)
          this.alertService.error(err.message);
    
        });
        this.loading = false;
      };
  //   this.authenticationService.login(this.model.username, this.model.password)
  //     .subscribe(
  //       data => {
  //         //  console.log(data);
  //         this.userService.update(data).subscribe(singleusers => { console.log(singleusers); }); //Update current user data with flag set as true for loggedin
  //         //Create login loginmetrics
  //         var loggedinCreate = { "firstName": data.firstName, "lastName": data.lastName, "userid": data._id, "Date": this.dateVar, "Status": "LoggedIn" };
  //         //loggedinCreate.push("test")
  //         this.userService.createmetrics(loggedinCreate).subscribe(data => { console.log('Data:' + data); });
  //         //End Metrics
  //         this.router.navigate([this.returnUrl]);
  //         //this.router.navigate([this.returnUrl]);
  //       },
  //       error => {
  //         this.alertService.error('Incorrect username or password, please try again');
  //         this.loading = false;
  //       });
  // }
}
