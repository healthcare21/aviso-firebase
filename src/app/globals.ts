import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  event: string = '95nkzTi62PAExDG8EAov';
  sitename: string = 'ZINFORO (ceftaroline fosamil) Virtual Discussion Forum';
  siteslogan: string = 'Discussion and resource center';
  moderation: string = 'moderated'; //mark on to moderate all posts - mark as moderated to moderate none. 
  adminFirstname: string = 'Jenni'; //Used in admin emails
  adminSurname: string = 'Strahan'; //Used in admin emails
  curDomain: string = 'http://aviso.healthcare21.website';
  adminEmail: string = 'aviso@healthcare21.co.uk';//Default email for admin based emails
  address: string = "Tytherington Business Park, Oakfield House, Springwood Way, Macclesfield, Cheshire, SK10 2XA";
  tel: string = "+44 (0)7932 497330";
  opening: string = "Technical support is available between 08:00 and 21:00 GMT and we will aim to resolve your query within 2 hours.";
  contactBusname: string = "HealthCare21";
  currentSite: string = "phizer"; //sanofi1, sanofi2, pfizer, demo
}
