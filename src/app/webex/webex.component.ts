import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals'
import { User } from '../_models/index';
import { UserService, EventsService, ForumsService } from '../_services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { IdleComponent } from '../idle/idle.component';
import { appConfig } from '../app.config';
import * as _ from "lodash";

@Component({
  selector: 'app-webex',
  templateUrl: './webex.component.html',
  styleUrls: ['./webex.component.scss']
})
export class WebexComponent implements OnInit {
  eventDetails: any;
  webexurl: any;
  allFiles: any;
  localURL: any;
  groups: any; allgroups: any;
  curIdfilter: any;
  currentUser: any;
  singleuser: any;
  forums:any; forumServ: any; showUpload: any; curId: any; forumsall: any;

  constructor(public globals: Globals, private EventsService: EventsService, private ForumsService: ForumsService, private UserService:  UserService,private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    //get embeded video and sanitize styleUrl
    this.curId = 'test'; this.curIdfilter = 'notset';
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.loadUser(this.currentUser['email']);
    this.loadAllEvents();
    this.localURL = appConfig.apiUrl;
  }

  toggleshowUpload(curId){
    this.curId = curId;
    this.showUpload = !this.showUpload;
    if(!this.showUpload){
    this.loadUser(this.currentUser['email']);
    }
  }

  private loadUser(curId) {
    this.UserService.getUserFire(curId).subscribe(singleusers => { 
        this.singleuser = singleusers;
        this.ForumsService.getGroupsFire().subscribe(groups => { this.groups = groups; this.allgroups = groups;  this.getUploads(groups); });
        this.forumServ = this.ForumsService.getallForumsfire().subscribe(forums => { this.filterByGroups(forums) });
    });
    
  }

  private loadAllEvents() {
    let eventsRet = [];
    this.EventsService.getAllEvents().subscribe(events => { 
      events.map(e => {
        eventsRet.push({'id':e.payload.doc.id, 'data': e.payload.doc.data()})
      })
      this.eventDetails = eventsRet;
    });
  }

  private filterName(id) {
  
    this.curIdfilter = id;
    // var allgroups = [...this.allgroups];
    // var newGroups = _.map(allgroups, function(o) {
    //   console.log('o',o.forumId, '   ID:   ',id);
    //   if (o.forumId === id) return o;
    // });

    // this.groups =  newGroups;

    // console.log('newGroups', this.groups );

  }

  private filterByGroups(forums){
    //Get users allowed groups
    let finalArr = []; let allForums = [];
    //Loop around forums and return only allowed forum groups
    var lock = false;
    for (var k = 0; k < forums.length; k++) {
      // console.log(forums[k].payload.doc.data());
      let forumarry1 = forums[k].payload.doc.data().groups;
      allForums.push (forums[k].payload.doc.data());
      if(forumarry1){
          if(forumarry1.length == 0){ finalArr.push(forums[k]); lock = true; } //Display forum if it has no groups assigned
          if(forumarry1[0].title === 'All'){
         
            if(!lock) {
              lock = true;
              finalArr.push(forums[k].payload.doc.data().groups); 
             };
          }
          if (!forums[k].payload.doc.data().groups){
            finalArr.push(forums[k].payload.doc.data().groups); //if no groups are set return the forum??   
            lock = true;
          } else {
              //Check to see if the user has the correct rights
            if(!forumarry1){
              if(!lock){ //No groups set presume all
                finalArr.push(forums[k]);
                lock = true;
              }
            } else {
              let array1 = forumarry1;
              let array2 = forums[k].payload.doc.data().groups;
              //Loop around users groups..
              for (var l = 0; l < array1.length; l++) {        
                var dif = _.findIndex(array2, (o) => { return _.isMatch(o, array1[l]) });
                if(dif != -1){
                  if(!lock){
                          finalArr.push(forums[k].payload.doc.data().groups);
                          lock = true;
                        }
                }
              }
              
            }
          }
      }
    }
    this.forums = finalArr;
    this.forumsall = allForums;
  }
  private getUploads(groups) {
    this.ForumsService.getUploadsFire().subscribe(files => { 
      this.groups = files;
      this.allgroups = files;
    })
  }
  
  private getUploadsSplit(groups) {
    var groupsN = _.values(groups);
    this.ForumsService.getUploadsFire().subscribe(files => { 
      //loop around each item and split into groups
      var arrN = _.values(files);
      var arrayLength = arrN.length; var arraylenGroup = groups.length;
      let newArray = [];
      for (var k = 0; k < arraylenGroup; k++) {
        //Setup return array with group name and each item
        newArray.push({groupName: groups[k].title, items: []});
      }
      
      for (var i = 0; i < arrayLength; i++) {
        // console.log(arrN[i]['groupArray'].length);
        for (var j = 0; j < arrN[i]['groupArray'][0].length; j++) {
          //Loop around the group array and push a new array
          for (var k = 0; k < arraylenGroup; k++) {
            if(arrN[i]['groupArray'][0][j].groupItem == newArray[k].groupName){
              newArray[k].items.push(arrN[i]);
            }
          }
        }

      }

      //loop around array and only add if the user has access..
      let finalArr = [];
      newArray = _.values(newArray);
      for (var l = 0; l < newArray.length; l++) {
   
        for (var m = 0; m < newArray.length; m++) {
        //Loop around User group access
        if(this.singleuser.group[m]){
          if(this.singleuser.group[m].title == 'All') {
            finalArr.push(newArray[l]);
          } else {
            if(this.singleuser.group[m].title == newArray[l].groupName ){
              finalArr.push(newArray[l]);
            }
        }
        }
      }
      }
      this.groups = finalArr;
      this.allgroups = finalArr;
    });
  }

}
