import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User, Event } from '../../_models/index';
import { UserService, AlertService, EventsService } from '../../_services/index';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss']
})
export class EventEditComponent implements OnInit {

  id: any;
  data: any;
  sub: any;
  users: any;
  events: any;
  singleusers: any;
  model: any = {};
  public myForm: FormGroup;

  constructor(private route: ActivatedRoute, private userService: UserService, private alertService: AlertService, private eventsService: EventsService, private _fb: FormBuilder, private router: Router, ) {
    this.createForm();

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.loadEvent(params['id']);
    });

  }
  removeLink(index) {
    const control = <FormArray>this.myForm.controls['usefulLinks'];
    control.removeAt(index);
  }

  removeResourceword(index) {
    const control = <FormArray>this.myForm.controls['wordlist'];
    control.removeAt(index);
  }

  removeResourceLink(index) {
    const control = <FormArray>this.myForm.controls['resources'];
    control.removeAt(index);
  }

  createForm() {
    this.myForm = this._fb.group({
      _id: '',
      name: '',
      location: '',
      webex: '',
      eventtext: '',
      loginText: '',
      inmoderation: '',
      enddate: '',
      usefulLinks: this._fb.array([
        //  this.initAbstracts(),
      ]),
      wordlist: this._fb.array([
        //  this.initAbstracts(),
      ])
    })
  }


  private loadEvent(curId) {
    //console.log(curId);
    this.eventsService.getSingleFire(curId).subscribe(event => { this.model = event; this.setInitialForm(event); });
  }

  setInitialForm(data) {
    this.myForm = this._fb.group({
      _id:  this.id,
      name: data.name,
      location: data.location,
      webex: data.webex,
      eventtext: data.eventtext,
      loginText: data.loginText,
      inmoderation: data.inmoderation,
      enddate: data.enddate,
      usefulLinks: this._fb.array([
        //  this.initAbstracts(),
      ]),
      wordlist: this._fb.array([
        //  this.initAbstracts(),
      ]),
      resources: this._fb.array([
        //  this.initAbstracts(),
      ]),
    })
    if (data.usefulLinks.length != 0) {
      for (let c = 0; c < data.usefulLinks.length; ++c) {
        this.addusefullinks(data.usefulLinks, c);
      }
    }
    if (data.resources.length != 0) {
      for (let c = 0; c < data.resources.length; ++c) {
        this.addresources(data.resources, c);
      }
    }
    if (data.wordlist.length != 0) {
      for (let c = 0; c < data.wordlist.length; ++c) {
        this.addwordlist(data.wordlist, c);
      }
    }
  }

  addwordlist(data, curIndex) {
    const control = <FormArray>this.myForm.controls['wordlist'];
    control.push(
      this._fb.group({
        wordlistName: data[curIndex].wordlistName
      })
    );
  }

  addresources(data, curIndex) {
    const control = <FormArray>this.myForm.controls['resources'];
    control.push(
      this._fb.group({
        resourceName: data[curIndex].resourceName,
        resourceLink: data[curIndex].resourceLink
      })
    );
  }

  addusefullinks(data, curIndex) {
    const control = <FormArray>this.myForm.controls['usefulLinks'];
    control.push(
      this._fb.group({
        linkname: data[curIndex].linkname,
        link: data[curIndex].link
      })
    );
  }

  addResource() {
    const control = <FormArray>this.myForm.controls['resources'];
    control.push(
      this._fb.group({
        resourceName: '',
        resourceLink: ''
      })
    );
  }

  addWord() {
    const control = <FormArray>this.myForm.controls['wordlist'];
    control.push(
      this._fb.group({
        wordlistName: ''
      })
    );
  }



  addLink() { //Function on click to add communication_objectives
    const control = <FormArray>this.myForm.controls['usefulLinks'];
    control.push(
      this._fb.group({
        linkname: '',
        link: ''
      })
    );

  }



  updateEvent() {

    this.eventsService.update(this.myForm.value).subscribe(singleusers => {
      this.alertService.success('Update successful', true);
      this.router.navigate(['/admin/users']);
    });
  }

}
