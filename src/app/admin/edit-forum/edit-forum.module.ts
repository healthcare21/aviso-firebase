import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForumAddComponent } from '../forum-add/forum-add.component';
import { EditForumComponent } from './edit-forum.component';
import { EventEditComponent } from '../event-edit/event-edit.component';
import { ForumModerationComponent } from '../forum-moderation/forum-moderation.component';
import { LikeCombinePipe } from '../../pipes/like-combine.pipe';
import { UserlookupPipe } from '../../pipes/userlookup.pipe';
import { LikesMetricsComponent } from '../likes-metrics/likes-metrics.component';
import { PostmetricsComponent } from '../postmetrics/postmetrics.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserreportComponent } from '../userreport/userreport.component';
import { DateFormatPipeAdmin } from '../../pipes/date-format-admin.pipe';
import { ExcludeuserPipe } from '../../pipes/excludeuser.pipe';
import { UsersComponent } from '../users/users.component';
import { routing } from './edit-forum.routing';
import { GroupAddComponent } from '../group-add/group-add.component';
import { GroupEditComponent } from '../group-edit/group-edit.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    routing
  ],
  declarations: [GroupEditComponent, UsersComponent, ExcludeuserPipe, DateFormatPipeAdmin, UserreportComponent, UserEditComponent, EditForumComponent,GroupAddComponent, ForumAddComponent, EventEditComponent, ForumModerationComponent, LikeCombinePipe, UserlookupPipe, LikesMetricsComponent, PostmetricsComponent]
})
export class EditForumModule { }
