import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  EventsService, ForumsService, AlertService, } from '../../_services/index';
import { Globals } from '../../globals'

import { FormBuilder, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-edit-forum',
  templateUrl: './edit-forum.component.html',
  styleUrls: ['./edit-forum.component.scss']
})
export class EditForumComponent implements OnInit {
  forums: any;
  webexurl: any;
  eventDetails: any;
  myForm: any;
  currentUser: any;
  sub: any;
  id: any;
  showForm: any;
  dateVar: any;
  showId: any;
  allReplies: any;
  groups: any;
  forumServ: any;
  repliesId: any;

  constructor(private ForumService: ForumsService, private _fb: FormBuilder, public globals: Globals, private EventsService: EventsService, private _sanitizer: DomSanitizer, private AlertService: AlertService, public router: Router, private route: ActivatedRoute) {
    this.createForm();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.showId = 1000000000000;
  }

  remove(index) {
    const control = <FormArray>this.myForm.controls['replies'];
    control.removeAt(index);
    this.showId = 1000000000000;
  }
  removerereply(index, replyindex) {
    var controlReply = this.myForm.controls['replies'];
    controlReply = controlReply.at(index);
    controlReply = controlReply.controls['replies'];
    controlReply.removeAt(replyindex);
  }
  hideme(index) {
    this.showId = index;
  }
  ngOnInit() {
    if (!this.currentUser) {
      this.router.navigate(['/']);
    }
    if (this.currentUser.authority != "admin") {
      this.router.navigate(['/']);
    }
    this.showForm = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
     // this.getEvent();
      this.getForums();
      this.dateVar = new Date();

    });
  }

  toggleform() {
    this.showForm = !this.showForm;
  }

  removeGroup(id){
    const control = <FormArray>this.myForm.controls['groups'];
    control.removeAt(id);
  }
  private getForums() {
    //get firebase forum posts
    let forumPass =[]; let allrepies = []; let arraysTotal =0;
    this.forumServ = this.ForumService.getallForumsfire().subscribe (forums => { 
      forums.map(e => {
        if(e.payload.doc.id == this.id){
          forumPass.push(e.payload.doc.data())
        
        }
      })

      this.repliesId = this.ForumService.getRepliesByID(this.id).subscribe (replies => {
        replies.map(e => {
          allrepies.push(e.payload.doc.data())
          arraysTotal ++;
        })
        this.allReplies = allrepies;
        forumPass[0]['replies'] = allrepies;
        this.forums = forumPass;
        this.setInitialForm(forumPass, allrepies);
      })
    
    
     
      //Get replies


     // this.setInitialForm(forumPass, arraysTotal);
    })
    
    
    // this.ForumService.getGroups().subscribe(groups => { this.groups = groups; });
    // this.ForumService.getSinglestatic(this.id).subscribe(forums => { this.forums = forums; 
    //   this.ForumService.getSinglereplyStatic(this.id).subscribe(replies => { this.allReplies = replies;   this.setInitialForm(this.forums, replies); });
    // });
  }
  private getEvent() {
  //  this.EventsService.getSingle(this.globals.event).subscribe(event => { this.eventDetails = event; this.webexurl = this._sanitizer.bypassSecurityTrustResourceUrl(this.eventDetails.webex) });
  }

  createForm() {
    this.myForm = this._fb.group({
      date: this.dateVar,
      _id: '',
      eventid: '',
      name: '',
      question: '',
      username: '',
      closeForum: '',
      groups: this._fb.array([
      ]),

      replies: this._fb.array([
      ])
    })
  }

  setInitialForm(data, replies) {
   
    this.myForm = this._fb.group({
      date: this.dateVar,
      _id: this.id,
      eventid: data[0].eventid,
      name: data[0].name,
      question: data[0].question,
      username: data[0].username,
      closeForum: data[0].closeForum,
      replies: this._fb.array([
      ]),
      groups: this._fb.array([
      ]),
    });
    const controlgroup = <FormArray>this.myForm.controls['groups'];
    if(data.groups){ //Only needed as we have old data.
    for (var j = 0; j < data.groups.length; j++) {
      controlgroup.push(
        this._fb.group({
          title:data.groups[j].title
        })
        )
       // console.log(this.groups[j].title)
    } 

    } else {
      //No groups set push all value
      controlgroup.push(
        this._fb.group({
          title: 'All'
        })
        )
    }
    // console.log('replies', data[0].replies);
    // if(data[0].replies  ){
    //   alert('4'+ data[0].replies.length);
    // }
    const control = <FormArray>this.myForm.controls['replies'];
    for (var i = 0; i < data[0].replies.length; i++) {
      control.push(
        this._fb.group({
           reply: data[0].replies[i].reply,
           username: data[0].replies[i].username,
           date: data[0].replies[i].date,
           authorref: data[0].replies[i].authorref,
        })
      )
      // this.addreplyReply(data, i);
    }
  }

  addGroup(){
    const control = <FormArray>this.myForm.controls['groups'];
    control.push(
      this._fb.group({
        title:['']
      })
    );
  }

  addreplyReply(data, i) {
    var controlReply = this.myForm.controls['replies'];
    controlReply = controlReply.at(i);
    controlReply = controlReply.controls['replies'];
    if (data.replies[i].replies) {
      for (var jj = 0; jj < data.replies[i].replies.length; jj++) {
        //controlReply.push(data.replies[i].replies[jj]);
        controlReply.push(
          this._fb.group({
            name: data.replies[i].replies[jj].name,
            reply: data.replies[i].replies[jj].reply,
            username: data.replies[i].replies[jj].username,
            date: data.replies[i].replies[jj].date,
            uniqueId: data.replies[i].replies[jj].uniqueId,
            authorref: data.replies[i].replies[jj].authorref,
            userid: data.userid,
            inmoderation: data.replies[i].replies[jj].inmoderation
          })
        );
        //console.log(data.replies[i].replies[jj]);
      }
    }
  }


  updateEvent() {
    this.ForumService.updatefire(this.myForm.value).subscribe(singleusers => {
      this.AlertService.success('Update successful', true);
      this.router.navigate(['/admin/users']);
    });
  }
  updateForm(username, name, reply) {
    const controlReply = <FormArray>this.myForm.controls['replies'];
    controlReply.push(
      this._fb.group({
        name: name.value,
        reply: reply.value,
        username: username.value,
        date: this.dateVar
      })
    );

    this.ForumService.update(this.myForm.value).subscribe(singleusers => {
      this.AlertService.success('Update successful', true);
      this.router.navigate(['/forum']);
    })

  }
}
