import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForumAddComponent } from '../forum-add/forum-add.component';
import { EditForumComponent } from './edit-forum.component';
import { EventEditComponent } from '../event-edit/event-edit.component';
import { ForumModerationComponent } from '../forum-moderation/forum-moderation.component';
import { PostmetricsComponent } from '../postmetrics/postmetrics.component';
import { LikesMetricsComponent } from '../likes-metrics/likes-metrics.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserreportComponent } from '../userreport/userreport.component';
import { UsersComponent } from '../users/users.component';
import { GroupAddComponent } from '../group-add/group-add.component';
import { GroupEditComponent } from '../group-edit/group-edit.component';

//these are all based on lazy loading to /admin route
const routes: Routes = [
  { path: 'forum/:id', component: EditForumComponent },
  { path: 'add-forum', component: ForumAddComponent },
  { path: 'event-edit/:id', component: EventEditComponent },
  { path: 'forum-moderation/:id', component: ForumModerationComponent },
  { path: 'likes', component: LikesMetricsComponent },
  { path: 'postmetrics', component: PostmetricsComponent },
  { path: 'users-edit/:id', component: UserEditComponent },
  { path: 'userreport', component: UserreportComponent },
  { path: 'users', component: UsersComponent },
  { path: 'group-add', component: GroupAddComponent },
  { path: 'group-edit/:id', component: GroupEditComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
