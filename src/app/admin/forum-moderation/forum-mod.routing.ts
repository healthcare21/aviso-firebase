import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForumModerationComponent } from './forum-moderation.component';

const routes: Routes = [
  { path: '', component: ForumModerationComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
