import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumModerationComponent } from './forum-moderation.component';

describe('ForumModerationComponent', () => {
  let component: ForumModerationComponent;
  let fixture: ComponentFixture<ForumModerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumModerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumModerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
