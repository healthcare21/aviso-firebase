import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, EventsService, ForumsService, AlertService } from '../../_services/index';
import { Globals } from '../../globals'
import { User, Event } from '../../_models/index';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  selector: 'app-forum-moderation',
  templateUrl: './forum-moderation.component.html',
  styleUrls: ['./forum-moderation.component.scss']
})
export class ForumModerationComponent implements OnInit {

  forums: any;
  webexurl: any;
  eventDetails: any;
  myForm: any;
  currentUser: any;
  sub: any;
  id: any;
  showForm: any;
  dateVar: any;

  constructor(private ForumService: ForumsService, private _fb: FormBuilder, public globals: Globals, private EventsService: EventsService, private _sanitizer: DomSanitizer, private AlertService: AlertService, public router: Router, private route: ActivatedRoute) {
    this.createForm();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.showForm = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      // this.getEvent();
      this.getForums();
      this.dateVar = new Date();

      $(document).ready(function() {

      });
    });
  }
  rebuild(index) {
    const control = this.myForm.controls['replies'];
    control.at(index).controls['inmoderation'].setValue('false');
    this.updateEvent();
  }
  rebuildSub(index, replyindex) {

    var control = this.myForm.controls['replies'];
    control = control.at(index).controls['replies'];
    control = control.at(replyindex).controls['inmoderation'];
    //console.log(control);
    control.setValue('false');
    this.updateEvent();
  }
  toggleform() {
    this.showForm = !this.showForm;
  }

  private getForums() {
    this.ForumService.getSinglefire(this.id).subscribe(forums => { this.setInitialForm(forums); this.getReplies(this.id); });
  }
  private getReplies(forumId) {
    this.ForumService.getRepliesByID(forumId).subscribe(forums => { this.forums = forums; this.setFormreplies(forums); });
    
  }

  createForm() {
    this.myForm = this._fb.group({
      _id: '',
      eventid: '',
      name: '',
      question: '',
      username: '',
      replies: this._fb.array([
      ])
    })
  }

  setInitialForm(data) {
    this.myForm = this._fb.group({
      _id: data._id,
      eventid: data.eventid,
      name: data.name,
      question: data.question,
      username: data.username,
      replies: this._fb.array([
      ])
    });
    const control = <FormArray>this.myForm.controls['replies'];
    for (var i = 0; i < data.replies.length; i++) {
      control.push(
        this._fb.group({
          name: data.replies[i].name,
          reply: data.replies[i].reply,
          username: data.replies[i].username,
          date: data.replies[i].date,
          inmoderation: data.replies[i].inmoderation,
          replies: this._fb.array([
          ])
        })
      )
      this.addreplyReply(data, i);
    }

  }

  setFormreplies(replies) {
    let returnedArr = [];
    for (var i = 0; i < replies.length; i++) {
      let retunedData = replies[i].payload.doc.data();
      retunedData.id = replies[i].payload.doc.id;
      returnedArr.push(retunedData);
    }
    this.forums = returnedArr;
    // console.log(this.forums);
  }

  approveMod(itemId){
    this.ForumService.acceptModeration(itemId);
  }

  addreplyReply(data, i) {
    var controlReply = this.myForm.controls['replies'];
    controlReply = controlReply.at(i);
    controlReply = controlReply.controls['replies'];
    if (data.replies[i].replies) {
      for (var jj = 0; jj < data.replies[i].replies.length; jj++) {
        //controlReply.push(data.replies[i].replies[jj]);
        controlReply.push(
          this._fb.group({
            name: data.replies[i].replies[jj].name,
            reply: data.replies[i].replies[jj].reply,
            username: data.replies[i].replies[jj].username,
            date: data.replies[i].replies[jj].date,
            inmoderation: data.replies[i].replies[jj].inmoderation
          })
        );
        //console.log(data.replies[i].replies[jj]);
      }
    }
  }


  updateEvent() {
    this.ForumService.update(this.myForm.value).subscribe(singleusers => {
      this.AlertService.success('Reply approved', true);
      //this.router.navigate(['/admin/users']);
    });
  }
  updateForm(username, name, reply, inmoderation) {
    const controlReply = <FormArray>this.myForm.controls['replies'];
    controlReply.push(
      this._fb.group({
        name: name.value,
        reply: reply.value,
        username: username.value,
        date: this.dateVar,
        inmoderation: inmoderation.value
      })
    );

    this.ForumService.update(this.myForm.value).subscribe(singleusers => {
      this.AlertService.success('Update successful', true);
      this.router.navigate(['/forum']);
    })

  }
}
