import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LikesMetricsComponent } from './likes-metrics.component';

describe('LikesMetricsComponent', () => {
  let component: LikesMetricsComponent;
  let fixture: ComponentFixture<LikesMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LikesMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikesMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
