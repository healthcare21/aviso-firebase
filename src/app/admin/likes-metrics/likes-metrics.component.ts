import { Component, OnInit } from '@angular/core';
import { UserService, AlertService, EventsService, ForumsService } from '../../_services/index';


@Component({
  selector: 'app-likes-metrics',
  templateUrl: './likes-metrics.component.html',
  styleUrls: ['./likes-metrics.component.scss']
})
export class LikesMetricsComponent implements OnInit {

  constructor(private ForumsService: ForumsService, private userService: UserService) { }

  totalLikesAll: any;
  forums: any;
  forumReturn: any;
  users: any;


  ngOnInit() {
    //Get all forums.
    this.forumReturn = [];
    this.getForums();
    this.totalLikesAll = 0;
  }

  private getForums() {
    this.ForumsService.getAllForumsstatic().subscribe(forums => { this.forums = forums; this.getAlllikes(forums); this.getForumlikes(forums) });
    this.userService.getAll().subscribe(users => { this.users = users; });

  }

  private getAlllikes(forums) {
    if (forums) {
      for (var x = 0; x < forums.length; x++) {
        if (forums[x].likes) {
          for (var y = 0; y < forums[x].likes.length; y++) {
            this.totalLikesAll++;
          }
        }

      }
    }
  }

  private getForumlikes(forums) {
    var forumlikes = [];
    var forumRows = [];
    var forumReturn = [];
    var cleanIndex = 0; var cleansubIndex = 0;

    if (forums) {
      for (var x = 0; x < forums.length; x++) {
        //console.log(forums[x].likes);
        //Ignore replies with no likes
        if (forums[x].likes) {
          forumReturn.push({ "forumName": forums[x].name, "forumId": forums[x]._id, "likes": [] });
          //console.log(forumReturn[cleanIndex]);
          //forumReturn[cleanIndex] = { "forumName": forums[x].name, "forumId": forums[x]._id, "likes": {} }
          // //console.log(forumReturn[cleanIndex]);
          // //loop around links and get replyname from replies within forum
          for (var y = 0; y < forums[x].likes.length; y++) {
            for (var z = 0; z < forums[x].replies.length; z++) {
              if (forums[x].likes[y].replyId == forums[x].replies[z].uniqueId) {
                //       cleansubIndex++;
                //  console.log(forumReturn[cleanIndex].likes);
                forumReturn[cleanIndex].likes.push({ "reply": forums[x].replies[z].reply, "user": forums[x].likes[y].userid, "replyId": forums[x].likes[y].replyId });
                //
              }
              if (forums[x].replies[z].replies) {
                //loop around replies of replies
                for (var a = 0; a < forums[x].replies[z].replies.length; a++) {
                  var readable = forums[x].replies[z].replies[a];
                  //  console.log(readable.uniqueId + ' : ' + forums[x].likes[y].replyId);
                  if (forums[x].likes[y].replyId == readable.uniqueId) {
                    cleansubIndex++;
                    forumReturn[cleanIndex].likes.push({ "reply": readable.reply, "user": forums[x].likes[y].userid, "replyId": forums[x].likes[y].replyId });
                  }
                }
              }
            }
          }
          cleanIndex++;

        }
        //Loop around each forum and get all likes
      }
      this.forumReturn = forumReturn;
    }

  }

  generateArray(obj) {
    return Object.keys(obj).map((key) => { return obj[key] });
  }

}
