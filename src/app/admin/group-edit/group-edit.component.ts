import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User, Event } from '../../_models/index';
import { UserService, AlertService, EventsService, ForumsService } from '../../_services/index';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as _ from "lodash";
@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.scss']
})
export class GroupEditComponent implements OnInit {

  groups: any; dateVar: any;  public myForm: FormGroup; sub:any; currentUser: any; id: any;
  constructor(private ForumService: ForumsService, private _fb: FormBuilder,  public router: Router, private route: ActivatedRoute, public alertService: AlertService) { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    if (!this.currentUser) {
      this.router.navigate(['/']);
    }
    if (this.currentUser.authority != "admin") {
      this.router.navigate(['/']);
    }

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      
    });

    this.createForm();
       this.loadAllGroups(this.id);
  
  }

  private loadAllGroups(id) {
    this.ForumService.getGroups().subscribe(groups => {
       this.groups = groups; 
        //split by id lodash
        var c = this.groups.filter(function(item) {
          return item._id === id;
          //console.log(item.forumId);
        });
        this.groups = c;
        this.setInitialForm(c[0]);
      });
  }

  createForm() {
    this.dateVar = new Date();
    this.myForm = this._fb.group({
      date: this.dateVar,
      title: ['']
    })
  }

  public creatGroup() {
    if (this.myForm.valid) {
      this.ForumService.updateGroup(this.myForm.value).subscribe(singleusers => {

        this.alertService.success('Update successful', true);
        this.router.navigate(['/admin/users']);
      });
    } else {  
      alert('Please enter all details for valid fields');
    }
  }
  

  setInitialForm(data) {
    this.myForm = this._fb.group({
      _id: data._id,
      title: data.title
    })
  }

}
