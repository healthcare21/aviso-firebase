import { Component, OnInit } from '@angular/core';
import { UserService, AlertService, EventsService, ForumsService } from '../../_services/index';

@Component({
  selector: 'app-postmetrics',
  templateUrl: './postmetrics.component.html',
  styleUrls: ['./postmetrics.component.scss']
})
export class PostmetricsComponent implements OnInit {

  constructor(private ForumsService: ForumsService) { }

  totalRepliesAll: any;
  totalReplies: any;
  totalRepliesReplies: any;
  totalPosts: any;
  eachForum: any;
  taggedTotaleach: any;
  forums: any;


  ngOnInit() {
    //Get all forums.
    this.getForums();
  }

  private getForums() {
    this.ForumsService.getAllForumsstatic().subscribe(forums => { this.forums = forums; this.getTotals(forums); this.buildForumwithTotals(forums); });
  }

  getTotals(forums) {
    //Loop around all forums initially bet replies total and then replies of repliesreplies
    var eachF = [];
    this.totalReplies = 0; this.totalRepliesAll = 0; this.totalRepliesReplies = 0; this.totalPosts = 0; this.taggedTotaleach = 0;
    for (var i = 0; i < forums.length; i++) {
      var replyreply = 0;
      eachF.push(forums[i]); // push all to eachF and then add totals
      this.totalPosts = forums.length;
      //Add initial replies length to totalReplies

      if (this.totalReplies + forums[i].replies) {


        this.totalReplies = this.totalReplies + forums[i].replies.length;
        this.totalRepliesAll = this.totalRepliesAll + forums[i].replies.length;
        eachF[i].repliesInitial = forums[i].replies.length;
        for (var j = 0; j < forums[i].replies.length; j++) {

          //Initial authorref
          if (forums[i].replies[j].authoref) {
            this.taggedTotaleach++;
          }
          for (var k = 0; k < forums[i].replies[j].replies.length; k++) {
            if (forums[i].replies[j].replies[k].authorref) {
              this.taggedTotaleach++;
            }
          }

          //Add replies to replies to totalReplies
          if (forums[i].replies[j].replies) {
            this.totalRepliesAll = this.totalRepliesAll + forums[i].replies[j].replies.length;
            this.totalRepliesReplies = this.totalRepliesReplies + forums[i].replies[j].replies.length;
            replyreply = replyreply + forums[i].replies[j].replies.length;

          }
        }
      }
      eachF[i].totalRepliesReplies = replyreply;
      eachF[i].totalReplies = replyreply + forums[i].replies.length;

    }
    this.eachForum = eachF;

  }
  buildForumwithTotals(forums) { //Awesome function name
    //^ Combined with above.
  }
  //Build array of forum posts with totals - Get Total replies on each posts


}
