import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostmetricsComponent } from './postmetrics.component';

describe('PostmetricsComponent', () => {
  let component: PostmetricsComponent;
  let fixture: ComponentFixture<PostmetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostmetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostmetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
