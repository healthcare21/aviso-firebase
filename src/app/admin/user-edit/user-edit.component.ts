import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User, Event } from '../../_models/index';
import { UserService, AlertService, EventsService, ForumsService } from '../../_services/index';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  id: any;
  userId: any;
  data: any;
  sub: any;
  //currentUser: User;
  users: any;
  singleUser: any;
  model: any = {};
  form: any;
  groups: any;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private ForumService: ForumsService, private userService: UserService, private alertService: AlertService, private router: Router, ) {

    this.createForm();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.loadUser(params['id']);
    });

  }
  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];

      if (file.size < 250000) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.form.get('avatar').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          });
        };
      } else {
        alert('Please keep your file size under 250kb');
      }
    }
  }

  createForm() {
    this.form = this.fb.group({
      _id: '',
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      // Don't updated password!!!!!!!!!!!  password: [''],
      biography: [''],
      username: ['', Validators.required],
      eventid: [''],
      authority: ['doctor'],
      avatar: null,
      group: this.fb.array([
        //  this.initAbstracts(),
      ]),
    });
  };

  populateForm(data) {
    this.form = this.fb.group({
      _id: data._id,
      firstName: [data.firstName, Validators.required],
      lastName: [data.lastName, Validators.required],
      email: [data.email, Validators.required],
      // Don't updated password!!!!!!!!!!!  password: [data.password],
      biography: [data.biography],
      username: [data.username, Validators.required],
      eventid: [data.eventid],
      authority: [data.authority],
      avatar: [data.avatar],
      group: this.fb.array([
        //  this.initAbstracts(),
      ]),
    });

   
    if(data.group){
        if (data.group.length != 0) {
          for (let c = 0; c < data.group.length; ++c) {
            this.addusegroup(data.group, c);
          }
        }
    } else {
      const control = <FormArray>this.form.controls['group'];
      control.push(
        this.fb.group({
          title: '',
    
        })
      );
    }
  }
  addGroup(){
    const control = <FormArray>this.form.controls['group'];
    control.push(
      this.fb.group({
        title:['All']
      })
    );
  }
  removeGroup(id){
    const control = <FormArray>this.form.controls['group'];
    control.removeAt(id);
  }

  private addusegroup(data, curIndex) {
    const control = <FormArray>this.form.controls['group'];
    control.push(
      this.fb.group({
        title: data[curIndex].title,
  
      })
    );
  }

  private loadUser(curId) {
    let groupsPass = [];
    this.ForumService.getGroupsFire().subscribe(groups => { 
      //remove payload
      groups.map(e => {
        groupsPass.push(e.payload.doc.data())
      })
      this.groups = groupsPass;
    
    });
    this.userService.getUserFire(curId).subscribe(singleusers => { 
       this.userId = singleusers.docs[0].id;
       this.model =singleusers.docs[0].data(); this.populateForm(singleusers.docs[0].data()); 
    });
  }
  updateUser() {
    this.userService.updateFire(this.form.value,this.userId);
      this.alertService.success('Update successful', true);
      localStorage.setItem('currentUser', JSON.stringify(this.form.value));
      this.router.navigate(['/admin/users']);
  }

}
