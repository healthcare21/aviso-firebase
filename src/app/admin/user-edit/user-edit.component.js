"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var index_1 = require("../../_services/index");
var UserEditComponent = /** @class */ (function () {
    function UserEditComponent(route, userService, alertService, router) {
        this.route = route;
        this.userService = userService;
        this.alertService = alertService;
        this.router = router;
        this.model = {};
    }
    UserEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id']; // (+) converts string 'id' to a number
            _this.loadUser(params['id']);
        });
    };
    UserEditComponent.prototype.loadUser = function (curId) {
        var _this = this;
        //console.log(curId);
        this.userService.getSingle(curId).subscribe(function (singleusers) { _this.model = singleusers; });
    };
    UserEditComponent.prototype.updateUser = function (curId) {
        var _this = this;
        this.userService.update(this.model).subscribe(function (singleusers) {
            _this.alertService.success('Update successful', true);
            _this.router.navigate(['/admin/users']);
        });
    };
    UserEditComponent = __decorate([
        core_1.Component({
            selector: 'app-user-edit',
            templateUrl: './user-edit.component.html',
            styleUrls: ['./user-edit.component.css']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, index_1.UserService, index_1.AlertService, router_1.Router])
    ], UserEditComponent);
    return UserEditComponent;
}());
exports.UserEditComponent = UserEditComponent;
//# sourceMappingURL=user-edit.component.js.map