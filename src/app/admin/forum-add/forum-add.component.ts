import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { User, Event } from '../../_models/index';
import { UserService, AlertService, ForumsService } from '../../_services/index';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Globals } from '../../globals';
// import * as $ from 'jquery';
declare var $: any;
@Component({
  selector: 'app-forum-add',
  templateUrl: './forum-add.component.html',
  styleUrls: ['./forum-add.component.scss']
})
export class ForumAddComponent implements OnInit {
  currentUser: User;
  dateVar: any;
  users: any;
  public myForm: FormGroup;
  constructor(private userService: UserService, private ForumService: ForumsService, private _fb: FormBuilder, public globals: Globals, private alertService: AlertService, private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.createForm();
  }

  ngOnInit() {
    this.loadAllUsers();
  }

  private loadAllUsers() {
    this.userService.getAll().subscribe(users => { this.users = users; });
  }

  createForm() {
    this.dateVar = new Date();
    this.myForm = this._fb.group({
      date: this.dateVar,
      lable: [''],
      name: ['', Validators.required],
      question: ['', Validators.required],
      username: this.currentUser.firstName + ' ' + this.currentUser.lastName,
      eventid: this.globals.event,
      replies: this._fb.array([
      ])
    })
  }

  private sendemail(email) {
    var bodyPP = {
      "to": email,
      "body": "Dear Doctor, <br/>A new topic has been posted on the " + this.globals.sitename + "<br/><br/>You can view the topic at: " + this.globals.curDomain,
      "subject": this.globals.sitename + " - New topic"
    };
    this.userService.sendemail(bodyPP).subscribe(event => { });
  }

  createForumpost() {
    if (this.myForm.valid) {

      let formWithGroup = this.myForm.value;
      formWithGroup.groups =[{title: 'All'}]
      //console.log(formWithGroup)

      this.ForumService.createfire(this.myForm.value)

      this.router.navigate(['/forum']);
      // this.ForumService.create(this.myForm.value).subscribe(singleusers => {
      //   //Loop around current users and send an email Regardless of optin options
      //   for (let i = 0; i < this.users.length; i++) {
      //     //this.sendemail(this.users[i].email);
      //   }
      //   this.alertService.success('Update successful', true);
      //   this.router.navigate(['/forum']);
      // });
    } else {
      alert('Please enter all details for valid fields');
    }
  }
}
