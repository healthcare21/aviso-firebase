import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForumAddComponent } from './forum-add.component';

const routes: Routes = [
  { path: '', component: ForumAddComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
