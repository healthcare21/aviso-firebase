import { Component, OnInit } from '@angular/core';
import { User, Event } from '../../_models/index';
import { UserService, AlertService, EventsService, ForumsService } from '../../_services/index';
import { Router } from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  currentUser: User;
  users:  any;
  events: any;
  forums: any;
  groups: any;

  constructor(private firebaseAuth: AngularFireAuth, private userService: UserService, private eventsService: EventsService, private router: Router, private ForumService: ForumsService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.currentUser.authority = 'admin'; //Remove this when a user with admin is set!!
    this.checkforAdmin(this.currentUser);
  }

  ngOnInit() {
    this.loadAllUsers();
    this.loadAllEvents();
    this.getForums();
    this.loadAllGroups();
  }
  checkforAdmin(currentUser) {
   // if (currentUser.authority != 'admin') { this.router.navigate(['/']); }
  }

  createUser(email, password){
    this.firebaseAuth
    .auth
    .createUserWithEmailAndPassword(email, password)
    .then(value => {
      console.log('Success!', value);
    })
    .catch(err => {
      console.log('Something went wrong:',err.message);
    });    
    }

  deleteUser(_id: string) {
    this.userService.delete(_id).subscribe(() => { this.loadAllUsers() });
  }

  private loadAllUsers() {
    this.userService.getAllFire().subscribe(users => { 
      let allUsers = [];
      for (var i = 0; i < users.length; i++) {
        allUsers.push(users[i].payload.doc.data());
        allUsers[i].id = users[i].payload.doc.id;
      }
      this.users = allUsers;

     });
  }

  private loadAllGroups() {
   let groupPass =[];
     this.ForumService.getallGroupsfire().subscribe (groups => { 
      // groups.map(e => {
      //   groupPass.push(e.payload.doc.data())
      // })
      this.groups = groups;
    })
  }

  private loadAllEvents() {
    let eventsRet = [];
    this.eventsService.getAllEvents().subscribe(events => { 
      events.map(e => {
        eventsRet.push({'id':e.payload.doc.id, 'data': e.payload.doc.data()})
      })
      this.events = eventsRet;
    });
  }
  private getForums() {
    // this.ForumService.getAllForumsstatic().subscribe(forums => { this.forums = forums; });
    this.ForumService.getallForumsfire().subscribe(forums => { 
      let forumPass =[];
      this.ForumService.getallForumsfire().subscribe (forums => { 
      forums.map(e => {
        forumPass.push({'id':e.payload.doc.id, 'data': e.payload.doc.data()})
      })
      this.forums = forumPass;
    });
      
      this.forums = forums; });
  }

}
