import { Component, OnInit } from '@angular/core';
import { User } from '../../_models/index';
import {  Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  AlertService, ForumsService } from '../../_services/index';

@Component({
  selector: 'app-group-add',
  templateUrl: './group-add.component.html',
  styleUrls: ['./group-add.component.scss']
})
export class GroupAddComponent implements OnInit {

  currentUser: User;
  dateVar: any;
  users: any;
  public myForm: FormGroup;


  constructor(private ForumService: ForumsService, private _fb: FormBuilder, private router: Router, private alertService: AlertService) { }



  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.dateVar = new Date();
    this.myForm = this._fb.group({
      date: this.dateVar,
      title: ['']
    })
  }

  creatGroup() {
    if (this.myForm.valid) {
      this.ForumService.createGroupfire(this.myForm.value);
      // this.ForumService.createGroup(this.myForm.value).subscribe(singleusers => {

      //   this.alertService.success('Update successful', true);
      //   this.router.navigate(['/admin/users']);
      // });
      this.alertService.success('Update successful', true);
       this.router.navigate(['/admin/users']);
    } else {  
      alert('Please enter all details for valid fields');
    }
  }

}
