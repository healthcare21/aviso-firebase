import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService, AlertService, EventsService, ForumsService } from '../../_services/index';
import * as moment from 'moment';

@Component({
  selector: 'app-userreport',
  templateUrl: './userreport.component.html',
  styleUrls: ['./userreport.component.scss']
})
export class UserreportComponent implements OnInit {

  currentUser: any;
  totalUsers: any;
  forums: any;
  users: any;
  usersResults: any;
  totalMetrics: any;
  retunedMetrics: any;
  allReplies: any;
  showIndex: any;
  forumsubscribe: any;
  forumsAll: any;

  constructor(private UserService: UserService, private ForumsService: ForumsService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.loadAllUsers();
    this.getForums();
  }

  ngOnInit() {
    //get all users
    this.allReplies = [];
    this.loadAllUsers();
    this.getForums();
  }
  ngOnDestroy() {
    if (this.forumsubscribe) {
      this.forumsubscribe.unsubscribe();
    }
  }


  private loadAllUsers() {
    this.UserService.getAll().subscribe(users => { this.users = users; this.totalUsers = this.users.length; this.userReportData(users); });
  }

  private getForums() {
    this.ForumsService.getAllForumsstatic().subscribe(forums => { this.forums = forums; this.getRepliesbyUser(forums); });
  }

  private getRepliesbyUser(forums) {
    var allreplies = [];
    for (var i = 0; i < forums.length; i++) {
      //loop around intial replies to get all replies
      for (var j = 0; j < forums[i].replies.length; j++) {
        var readableVar = forums[i].replies[j];
        delete readableVar.replies;
        allreplies.push(readableVar);

        if (allreplies) {
          allreplies[allreplies.length - 1].forumId = forums[i]._id;
          this.forumsubscribe = this.ForumsService.getSinglestatic(forums[i]._id).subscribe(forumsNew => {

            var forumsAll = [];
            forumsAll.push(forumsNew);

            for (var k = 0; k < forumsAll[0].replies.length; k++) {
              for (var l = 0; l < forumsAll[0].replies[k].replies.length; l++) {

                allreplies.push(forumsAll[0].replies[k].replies[l]);
                allreplies[allreplies.length - 1].forumId = forumsAll[0]._id;

              }
            }
            this.allReplies = allreplies;


          });

        }

      }

    }

  }

  private setReplyshow(index) {
    if (this.showIndex != index) {
      this.showIndex = index;
    } else {
      this.showIndex = 100000;
    }
  }
  private userReportData(users) {
    //get data for all logins

    this.retunedMetrics = []; var metricsArrr = [];//Pirate metrics Arrrrr
    this.UserService.getMetrics().subscribe(metrics => {
      this.totalMetrics = metrics;
      for (var i = 0; i < users.length; i++) {
        this.retunedMetrics.push(users[i]);
        this.retunedMetrics[i].combinedName = users[i].firstName + ' ' + users[i].lastName;
        this.retunedMetrics[i].usermetrics = [];
        for (var j = 0; j < this.totalMetrics.length; j++) {
          if (metrics[j].userid == users[i]._id) {
            this.retunedMetrics[i].usermetrics.push(metrics[j]);
          }
        }
      }
      //  Loop around returnedMetrics - usermetrics and get time difference
      for (let k = 0; k < this.retunedMetrics.length; k++) {
        let totalTime = 0;
        if (this.retunedMetrics[k].usermetrics) {
          for (let l = 0; l < this.retunedMetrics[k].usermetrics.length; l++) {
            var tooComplextoRead = this.retunedMetrics[k].usermetrics[l];
            var nextIndex = l + 1;
            var nextDateStatus = this.retunedMetrics[k].usermetrics[nextIndex];
            if (tooComplextoRead.Status == 'LoggedIn') {
              var firstdate = tooComplextoRead.Date;
              if (nextDateStatus) {//This shouldn't be neccessary but just in case end of line
                if (nextDateStatus.Status == 'LoggedOut') {
                  //Work out the difference in moments and set array value to timeLogged -diiference
                  var secondDate = moment(nextDateStatus.Date);
                  //difference between dates in minutes
                  var now = moment(firstdate);
                  var secondN = now.diff(secondDate, 'seconds');
                  secondN = secondN * -1;
                  totalTime = totalTime + secondN;
                  var minutes = Math.floor(secondN / 60);
                  var seconds = secondN - minutes * 60;
                  //add to array the difference
                  tooComplextoRead.minuteDiffernce = minutes + ':' + seconds;
                } else {
                  //  console.log('They forgot to Log out');  //Should not get here as the user will still be Loggedin when they return to the platform
                  tooComplextoRead.minuteDiffernce = 'Forgot to log out';
                }
              } else {
                tooComplextoRead.minuteDiffernce = 'Still logged in';
              }
            }
          }
        }
        var totalminutes = Math.floor(totalTime / 60);
        var seconds = totalTime - totalminutes * 60;
        this.retunedMetrics[k].totalTime = totalminutes + ':' + seconds;
      }
      //  console.log(this.retunedMetrics);
    });
  }
}
