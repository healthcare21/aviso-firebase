import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-replysanitize',
  templateUrl: './replysanitize.component.html',
  styleUrls: ['./replysanitize.component.scss']
})
export class ReplysanitizeComponent implements OnInit {
  @Input() reply;
  @Input() references;
  @Input() allforums;
  @Input() authorRef;
  @Input() currentUser;
  @Input() postUser;
  @Input() replyId;
  @Input() uniqueId;
  returnReferences: any;
  returnclick: any;
  closeResult: any;
  replyrr: any; replyid: any; replyname: any;
  currenName: any;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.currenName = this.currentUser.firstName + " " + this.currentUser.lastName;
    this.returnReferences = [];
    //Split reference array by comma
    if (this.references) {
      var substring = ",";
      if (this.references.includes(substring)) {
        var res = this.references.split(",");
      } else {
        var res = this.references.split(",");
      }
      if (this.allforums) {
        for (var i = 0; i < res.length; i++) {
          //loop around forum arrays and find references
          for (var j = 0; j < this.allforums.replies.length; j++) {
            if (res[i] == this.allforums.replies[j].uniqueId) {
              var stringR = [this.allforums.replies[j].reply, this.allforums.replies[j].uniqueId, this.allforums.replies[j].username];
              this.returnReferences.push(stringR);
            }
            //Loop around Replies of Replies
            if (this.allforums.replies[j].replies) {
              for (var k = 0; k < this.allforums.replies[j].replies.length; k++) {
                if (res[i] == this.allforums.replies[j].replies[k].uniqueId) {
                  var tooUnreadable = this.allforums.replies[j].replies[k];
                  var stringR = [tooUnreadable.reply, tooUnreadable.uniqueId, tooUnreadable.username];
                  this.returnReferences.push(stringR);
                }
              }
            }
          }
        }
      }
    } //End if Reference
  }



  open(content, reply, replyId, replyname) {
    this.replyrr = reply; this.replyid = replyId, this.replyname = replyname;
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
