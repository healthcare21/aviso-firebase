import { NgModule } from '@angular/core';
import { ReplysanitizeComponent } from './replysanitize.component';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [ReplysanitizeComponent],
  imports: [CommonModule],
  exports: [ReplysanitizeComponent]
})
export class ReplysanitizeModule { }
