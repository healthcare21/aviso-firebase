import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplysanitizeComponent } from './replysanitize.component';

describe('ReplysanitizeComponent', () => {
  let component: ReplysanitizeComponent;
  let fixture: ComponentFixture<ReplysanitizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplysanitizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplysanitizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
